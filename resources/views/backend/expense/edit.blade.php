@extends('admin.layouts.admin_design')

@section('title')
<title>Edit Employee - Jane Link Travel</title> 
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     Edit Employee
   </h1>
   <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
     <li class="breadcrumb-item"><a href="{{route('employee.index')}}">View All</a></li>
     <li class="breadcrumb-item active">Edit Employee</li>
   </ol>
 </section>

 <section class="content">

  <!-- Basic Forms -->
   <div class="box box-solid box-info">
     <div class="box-header with-border">
       <h6 class="box-subtitle text-white"> Employee Details </h6>
     </div>
     <!-- /.box-header -->
     <div class="box-body">
       <div class="row">
         <div class="col">
          <form action="{{route('expense.update',$expenses->id)}}" method="post" enctype="multipart/form-data">
             @csrf

          <div class="form-group">
         <h5>Date&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
          <?php $dv = Carbon\Carbon::today(); ?>
           <input type="date" name="expense_date" id="date" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $dv->toDateString() }}"> </div>
       </div>   
       <div class="form-group">
         <h5>Title&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="expense_title" id="title" class="form-control" data-validation-required-message=" Title Field is required" value="{{$expenses->expense_title}}"> </div>
       </div>

       <div class="form-group">
         <h5>Price&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="expense_price" id="price" class="form-control" data-validation-required-message=" Title Field is required" value="{{$expenses->expense_price}}"> </div>
       </div>
      <div class="form-group">
            <h5>Select Expenses by&nbsp;<span class="text-danger">*</span></h5>
            <select name="employee_id" id="employee_id" class="form-control">
                @foreach($employee as $employee_data)                
                
                <option value="{{ $employee_data->id }}">{{ $employee_data->employee_name }}</option>
                @endforeach
              

            </select>
        </div>

         
        <div class="form-group">
          <h5>Description</h5>
          <div class="controls">
              <textarea id="editor1" name="description" rows="10" cols="80">
{!!$expenses->description !!}
              </textarea>
                           
        </div>
       </div>

          
        <br>
        <div class="text-xs-right text-center">
            <button type="submit" class="btn btn-info btn-lg">Submit</button>
          </div>  
     </form>

         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </div>
     <!-- /.box-body -->
   </div>
   <!-- /.box -->

 </section>
 <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
      $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
    </script>

    <script type="text/javascript">
        @if(session('flash_message'))
 swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
    <script type="text/javascript">
      $('.yyy').click(function(){
        console.log('hello')
        if('button[class=active]'){
          $('#abc').val('T')
        }
        else{
           $('#abc').val('F')
        }
      });
    </script>
    <script type="text/javascript">
      
      

$(function() {
    $("#salary1, #salary2, #salary3" ).on("keydown keyup", sum);
  function sum() {
  $("#salarysum").val(Number($("#salary1").val()) + Number($("#salary2").val()) + Number($("#salary3").val()));
  }
});
    </script>
@endsection
