@extends('admin.layouts.admin_design')

@section('title')
<title>Edit Employee - Jane Link Travel</title> 
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     Edit Employee
   </h1>
   <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
     <li class="breadcrumb-item"><a href="{{route('employee.index')}}">View All</a></li>
     <li class="breadcrumb-item active">Edit Employee</li>
   </ol>
 </section>

 <section class="content">

  <!-- Basic Forms -->
   <div class="box box-solid box-info">
     <div class="box-header with-border">
       <h6 class="box-subtitle text-white"> Employee Details </h6>
     </div>
     <!-- /.box-header -->
     <div class="box-body">
       <div class="row">
         <div class="col">
             <form action="{{route('employee.update',$employee->id)}}" method="post" enctype="multipart/form-data">
             @csrf
       <div class="form-group">
         <h5>Employee Full Name:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="employee_name" id="name" class="form-control" data-validation-required-message=" Title Field is required" value="{{$employee->employee_name}}"> </div>
       </div>

        <div class="form-group">
         <h5>Age:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="age" id="age" class="form-control age" data-validation-required-message=" Title Field is required" value="{{$employee->age}}"> </div>
       </div>

        <div class="form-group">
         <h5>Date of Birth::&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="dob" id="dob" readonly="true" class="form-control age" data-validation-required-message=" Title Field is required" value="{{$employee->dob}}"> </div>
       </div>

       <div class="form-group">
            <h5>Upload Image <span class="text-danger">*</span></h5>
            <div class="controls">
                <input type="file" name="employee_image" class="form-control"> 
                <input type="hidden" name="current_image" class="form-control" value="{{ $employee->employee_image }}"> 
                <img src="{{ asset('public/uploads/employee/verysmall/'.$employee->employee_image) }}">
            </div>
        </div>

          <div class="form-group">
            <h5>Select Gender: &nbsp;<span class="text-danger">*</span></h5>
            <select name="gender" id="gender" class="form-control">                
                <option value="male">Male</option>
                <option value="male">Female</option>
                <option value="male">Other</option>
            </select>
        </div>
        <div class="form-group">
         <h5>Employee Address:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="employee_address" id="address" class="form-control" data-validation-required-message=" Title Field is required" value="{{$employee->employee_address}}"> </div>
       </div>

        <div class="form-group">
         <h5>Employee Email:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="email" name="employee_email" id="email" class="form-control" data-validation-required-message=" Title Field is required" value="{{$employee->employee_email}}"> </div>
       </div>

        <div class="form-group">
         <h5>Employee Phone No.&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="employee_phone" id="phone" class="form-control" data-validation-required-message=" Title Field is required" value="{{$employee->employee_phone}}"> </div>
       </div>

       <div class="form-group">
         <h5>Employee Passport No.&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="employee_passport" id="passport" class="form-control" data-validation-required-message=" Title Field is required" value="{{$employee->employee_passport}}"> </div>
       </div>


       <div class="form-group">
         <h5>Hired Date:.&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="date" name="hired_date" id="date" class="form-control" data-validation-required-message=" Title Field is required" value="{{$employee->hired_date}}"> </div>
       </div>
        
        <div class="form-group">
          <h5>Description:</h5>
          <div class="controls">
              <textarea id="editor1" name="description" rows="10" cols="80">
        {!!$employee->description !!}
              </textarea>
                           
        </div>
       </div>

          
        <br>
        <div class="text-xs-right text-center">
            <button type="submit" class="btn btn-info btn-lg">Submit</button>
          </div>  
     </form>

         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </div>
     <!-- /.box-body -->
   </div>
   <!-- /.box -->

 </section>
 <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
      $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
    </script>

    <script type="text/javascript">
        @if(session('flash_message'))
 swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
    <script type="text/javascript">
      $('.yyy').click(function(){
        console.log('hello')
        if('button[class=active]'){
          $('#abc').val('T')
        }
        else{
           $('#abc').val('F')
        }
      });
    </script>
   

     <script type="text/javascript">      
        $('.age').change(function(){
         var age = document.getElementById('age').value
         var bDay = moment().subtract(age, 'years');
         var bvalue =moment(bDay).format('MM/DD/YYYY');
         console.log(bvalue);
         document.getElementById('dob').value = bvalue;      
        });
      </script>
@endsection
