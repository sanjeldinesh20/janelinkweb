@extends('admin.layouts.admin_design')

@section('title')
<title>Add service - Jane Link Travel</title> 
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     Add New service
   </h1>
   <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
     <li class="breadcrumb-item"><a href="{{route('service.index')}}">View All</a></li>
     <li class="breadcrumb-item active">Add service</li>
   </ol>
 </section>

 <section class="content">

  <!-- Basic Forms -->
   <div class="box box-solid box-info">
     <div class="box-header with-border">
       <h6 class="box-subtitle text-white"> service Details </h6>
     </div>
     <!-- /.box-header -->
     <div class="box-body">
       <div class="row">
         <div class="col">
           <form action="{{route('service.store')}}" method="post" enctype="multipart/form-data">
             @csrf
       <div class="form-group">
         <h5>Service Full Name&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="service_name" id="service_name" class="form-control" data-validation-required-message=" Title Field is required" value=""> </div>
       </div>
   
         
        <div class="form-group">
          <h5>Description</h5>
          <div class="controls">
              <textarea id="editor1" name="description" rows="10" cols="80">

              </textarea>
              {{-- <iframe width="100%" height="400" frameborder="0" border="no" scrolling="no" marginwidth="0" marginheight="0" allowtransparency="true" src="https://www.ashesh.com.np/linknepali-unicode3.php?api=730136j091">
              </iframe><br> --}}
              {{-- <span style="color:gray; font-size:8px; text-align:left">© <a href="https://www.ashesh.com.np/nepali-unicode.php" title="Nepali unicode" target="_top" style="text-decoration:none; color:gray;">Nepali Unicode</a></span> --}}
        </div>
       </div>

          
        <br>
        <div class="text-xs-right text-center">
            <button type="submit" class="btn btn-info btn-lg">Submit</button>
          </div>  
     </form>

         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </div>
     <!-- /.box-body -->
   </div>
   <!-- /.box -->

 </section>
 <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
			$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
		}(window, document, jQuery);
    </script>

    <script type="text/javascript">
        @if(session('flash_message'))
 swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
    <script type="text/javascript">
      $('.yyy').click(function(){
        console.log('hello')
        if('button[class=active]'){
          $('#abc').val('T')
        }
        else{
           $('#abc').val('F')
        }
      });
    </script>
@endsection
