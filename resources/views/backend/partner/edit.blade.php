@extends('admin.layouts.admin_design')

@section('title')
<title>Add Partner - Jane Link Travel</title> 
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     Add New Partner
   </h1>
   <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
     <li class="breadcrumb-item"><a href="{{route('partner.index')}}">View All</a></li>
     <li class="breadcrumb-item active">Add Partner</li>
   </ol>
 </section>

 <section class="content">

  <!-- Basic Forms -->
   <div class="box box-solid box-info">
     <div class="box-header with-border">
       <h6 class="box-subtitle text-white"> Partner Details </h6>
     </div>
     <!-- /.box-header -->
     <div class="box-body">
       <div class="row">
         <div class="col">
           <form action="{{route('partner.update',$partner->id)}}" method="post" enctype="multipart/form-data">
             @csrf
       <div class="form-group">
         <h5>Partner Name&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="partner_name" id="partner_name" value="{{$partner->partner_name}}" class="form-control" data-validation-required-message=" Title Field is required" value=""> </div>
       </div>
     {{-- <div class="form-group">
            <h5>Upload Image <span class="text-danger">*</span></h5>
            <div class="controls">
                <input type="file" name="partner_image" class="form-control"> 
                <input type="hidden" name="current_image" class="form-control" value="{{ $partner->image }}"> 
                <img src="{{ asset('public/uploads/partner/verysmall/'.$partner->partner_image) }}">
            </div>
        </div>--}}

       <div class="form-group">
        <h5>Partner Email&nbsp;<span class="text-danger"></span></h5>
        <div class="controls">
              <input type="email" name="partner_email" value="{{$partner->partner_email}}" class="form-control" > </div>
      </div>
      <div class="form-group">
         <h5>Partner Adddress&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="partner_address" id="address" value="{{$partner->partner_address}}" class="form-control" data-validation-required-message=" Title Field is required" value=""> </div>
       </div>
        <div class="form-group">
         <h5>Partner Contact No&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="partner_contact" id="contact" value="{{$partner->partner_contact}}" class="form-control" data-validation-required-message=" Title Field is required" value=""> </div>
       </div>

         <div class="form-group">
            <h5>Select Service Type&nbsp;<span class="text-danger">*</span></h5>
            <select name="service_id" id="service_id" class="form-control">
                @foreach($service as $service_data)                
                <option value="{{ $service_data->id }}">{{ $service_data->service_name }}</option>
                @endforeach
              

            </select>
        </div>
    
    
        <div class="form-group">
          <h5>Description</h5>
          <div class="controls">
              <textarea id="editor1" name="description" rows="10" cols="80">
      {!!$partner->description!!}
              </textarea>
              {{-- <iframe width="100%" height="400" frameborder="0" border="no" scrolling="no" marginwidth="0" marginheight="0" allowtransparency="true" src="https://www.ashesh.com.np/linknepali-unicode3.php?api=730136j091">
              </iframe><br> --}}
              {{-- <span style="color:gray; font-size:8px; text-align:left">© <a href="https://www.ashesh.com.np/nepali-unicode.php" title="Nepali unicode" target="_top" style="text-decoration:none; color:gray;">Nepali Unicode</a></span> --}}
        </div>
       </div>

          
        <br>
        <div class="text-xs-right text-center">
            <button type="submit" class="btn btn-info btn-lg">Submit</button>
          </div>  
     </form>

         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </div>
     <!-- /.box-body -->
   </div>
   <!-- /.box -->

 </section>
 <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
      $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
    </script>

    <script type="text/javascript">
        @if(session('flash_message'))
 swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
    <script type="text/javascript">
      $('.yyy').click(function(){
        console.log('hello')
        if('button[class=active]'){
          $('#abc').val('T')
        }
        else{
           $('#abc').val('F')
        }
      });
    </script>
@endsection
