@extends('admin.layouts.admin_design')

@section('title')
<title>View Partner - Jane Link Travel</title> 
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>

            <button class="btn  btn-lg btn-info" ><a href="{{route('partner.create')}}">  Add Partner</a></button>
        </h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
          <li class="breadcrumb-item"><a href="{{route('partner.create')}}">Add New</a></li>
          <li class="breadcrumb-item active">View</li>
        </ol>
      </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
         
         <div class="box box-solid box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">Partner Details</h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>S.N</th>
							<th>Partner Name</th>
							{{--<th>Partner Image</th>--}}
              <th>Partner Contact</th>
              <th>Partner Address</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
            @foreach($partner as $partner_data)           
						<tr>
							<td>{{ $loop->index+1 }}</td>
              <td>{{ $partner_data->partner_name }}</td>
              {{--<td><img src="{{asset('public/uploads/partner/verysmall/'.$partner_data->partner_image) }}" alt="{{ $partner_data->title }}" width="50"></td>--}}
              <td>{{ $partner_data->partner_contact }}</td>
              <td>{{ $partner_data->partner_address }}</td>
							<td>
                <a href="{{ route('partner.edit',$partner_data->id) }}" title="Edit" data-toggle="tooltip">
                    <i class="fas fa-edit fa-2x text-info"></i> 
                </a>
                &nbsp;&nbsp;
                <a  href="javascript:" rel="{{$partner_data->id}}" rel1="partner/destroy" class="text-inverse deleteRecord" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                    <i class="fas fa-trash-alt fa-2x text-danger"></i>
                </a>
              </td>
            </tr>
            @endforeach
           
					</tbody>
				
				  </table>
				</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
                  
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection



@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
			$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
		}(window, document, jQuery);
    </script>


    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>

@endsection