@extends('admin.layouts.admin_design')

@section('title')
<title> Setting - Jane link</title>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
      Setting
   </h1>
   <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
     <li class="breadcrumb-item"><a href="javascript:"> Setting</a></li>
     <li class="breadcrumb-item active">Update</li>
   </ol>
 </section>

 <!-- Main content -->
 <section class="content">

  <!-- Basic Forms -->
   <div class="box box-solid box-info">
     <div class="box-header with-border">
       <h6 class="box-subtitle text-white"> Setting's Details</h6>
     </div>
     <!-- /.box-header -->
     <div class="box-body">
       <div class="row">
         <div class="col">
           <form action="{{route('sitesetting.update', $sites->id)}}" method="post" enctype="multipart/form-data">
             @csrf
            <div class="form-group">
              <h5>Organization Name<span class="text-danger">*</span></h5>
              <div class="controls">
                <input type="text" name="organization_name" id="organization_name" class="form-control" data-validation-required-message="This field is required" value="{{$sites->organization_name}}"> </div>
            </div>
             <div class="form-group">
              <h5>Organization Logo</h5>
              <div class="controls">
                <input type="file" name="organization_logo" class="form-control"> </div>
                <br>
                <input type="hidden" name="current_logo" value="{{$sites->organization_logo}}">
                <img src="{{asset('public/uploads/setting/small/'.$sites->organization_logo)}}" alt="{{$sites->organization_name}}" width="100px">
            </div>
            <div class="form-group">
              <h5>Address<span class="text-danger">*</span></h5>
              <div class="controls">
                <input type="text" name="organization_address" id="address" class="form-control" data-validation-required-message="This field is required" value="{{$sites->organization_address}}"> </div>
            </div>

            <div class="form-group">
              <h5>Email<span class="text-danger">*</span></h5>
              <div class="controls">
                <input type="email" name="organization_email" id="email" class="form-control"  data-validation-required-message="This field is required" value="{{$sites->organization_email}}"> </div>
            </div>
            
            <div class="form-group">
              <h5>Mobile Number<span class="text-danger">*</span></h5>
              <div class="controls">
                <input type="text" name="contact_number" id="contact_number" class="form-control"  data-validation-required-message="This field is required" value="{{$sites->contact_number}}"> </div>
            </div>

            <div class="form-group">
              <h5>Chief<span class="text-danger">*</span></h5>
              <div class="controls">
                <input type="text" name="chief" id="chief" class="form-control"  data-validation-required-message="This field is required" value="{{$sites->chief}}"> </div>
            </div>

           <div class="form-group">
              <h5>Director</h5>
              <div class="controls">
                <input type="text" name="director" id="director" class="form-control" data-validation-required-message="This field is required" value="{{$sites->director}}"> </div>
            </div>

             <div class="form-group">
              <h5>Tax</h5>
              <div class="controls">
                <input type="number" name="tax" id="tax" class="form-control" data-validation-required-message="This field is required" value="{{$sites->tax}}"> </div>
            </div>

         
            <div class="form-group">
              <h5>Facebook</h5>
              <div class="controls">
                <input type="text" name="organization_facebook" id="organization_facebook" class="form-control" data-validation-required-message="This field is required" value="{{$sites->organization_facebook}}"> </div>
            </div>

            <div class="form-group">
              <h5>Twitter</h5>
              <div class="controls">
                <input type="text" name="organization_twitter" id="organization_twitter" class="form-control" data-validation-required-message="This field is required" value="{{$sites->organization_twitter}}"> </div>
            </div>
            
            <div class="form-group">
              <h5>Linkedin</h5>
              <div class="controls">
                <input type="text" name="organization_linkedin" id="organization_linkedin" class="form-control" data-validation-required-message="This field is required" value="{{$sites->organization_linkedin}}"> </div>
            </div>

            <div class="form-group">
              <h5>Instagram</h5>
              <div class="controls">
                <input type="text" name="organization_instagram" id="organization_instagram" class="form-control" data-validation-required-message="This field is required" value="{{$sites->organization_instagram}}"> </div>
            </div>

          <div class="text-xs-right text-center">
            <button type="submit" class="btn btn-info btn-lg">Submit</button>
          </div>  
  
        </form>

         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </div>
     <!-- /.box-body -->
   </div>
   <!-- /.box -->

 </section>
 <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
			$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
		}(window, document, jQuery);
    </script>


    <script type="text/javascript">
        @if(session('flash_message'))
 swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
@endsection
