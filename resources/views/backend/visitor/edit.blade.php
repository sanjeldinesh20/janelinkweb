@extends('admin.layouts.admin_design')

@section('title')
<title>Edit Visitor - Jane Link Travel</title> 
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     Edit New Visitor
   </h1>
   <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
     <li class="breadcrumb-item"><a href="{{route('visitor.index')}}">View All</a></li>
     <li class="breadcrumb-item active">Edit visitor</li>
   </ol>
 </section>

 <section class="content">

  <!-- Basic Forms -->
   <div class="box box-solid box-info">
     <div class="box-header with-border">
       <h6 class="box-subtitle text-white"> Visitor Details </h6>
     </div>
     <!-- /.box-header -->
     <div class="box-body">
       <div class="row">
         <div class="col">
           <form action="{{route('visitor.update',$visitor->id)}}" method="post" enctype="multipart/form-data">
             @csrf
       <div class="form-group">
         <h5>Visitor Full Name&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="visitor_name" id="visitor_name" class="form-control" data-validation-required-message=" Title Field is required" value="{{$visitor->visitor_name }}"> </div>
       </div>
     <div class="form-group">
            <h5>Select Service Type&nbsp;<span class="text-danger">*</span></h5>
            <select name="country_id" id="country_id" class="form-control">
                @foreach($country as $country_data)                
                <option value="{{ $country_data->id }}">{{ $country_data->name }}</option>
                @endforeach

            </select>
        </div>

       <div class="form-group">
        <h5>Visitor Email&nbsp;<span class="text-danger">*</span></h5>
        <div class="controls">
              <input type="email" name="visitor_email" value="{{$visitor->visitor_email}}" class="form-control" required data-validation-required-message="Email  field is required"> </div>
      </div>

      <div class="form-group">
         <h5>Visitor Adddress&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="visitor_address" id="address" value="{{$visitor->visitor_address}}" class="form-control" data-validation-required-message=" Title Field is required" value=""> </div>
       </div>
        <div class="form-group">
         <h5>Visitor Phone No&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="visitor_contact" id="contact" class="form-control" data-validation-required-message=" Title Field is required" value="{{$visitor->visitor_contact }}"> </div>
       </div>
     
      <div class="form-group">
         <h5>Passport Number&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="visitor_passport" id="visitor_passport" value="{{$visitor->visitor_passport }}" class="form-control" data-validation-required-message=" Title Field is required" value=""> </div>
       </div>

        <div class="form-group">
            <h5>Select Service Type&nbsp;<span class="text-danger">*</span></h5>
            <select name="service_id" id="service_id" class="form-control">
                @foreach($service as $service_data)                
                <option value="{{ $service_data->id }}">{{ $service_data->service_name }}</option>
                @endforeach
              
            </select>
        </div>
        <div class="form-group">
          <h5>Description</h5>
          <div class="controls">
              <textarea id="editor1" name="description" rows="10" cols="80">
{!!$visitor->description !!}
              </textarea>
              {{-- <iframe width="100%" height="400" frameborder="0" border="no" scrolling="no" marginwidth="0" marginheight="0" allowtransparency="true" src="https://www.ashesh.com.np/linknepali-unicode3.php?api=730136j091">
              </iframe><br> --}}
              {{-- <span style="color:gray; font-size:8px; text-align:left">© <a href="https://www.ashesh.com.np/nepali-unicode.php" title="Nepali unicode" target="_top" style="text-decoration:none; color:gray;">Nepali Unicode</a></span> --}}
        </div>
       </div>

          
        <br>
        <div class="text-xs-right text-center">
            <button type="submit" class="btn btn-info btn-lg">Submit</button>
          </div>  
     </form>

         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </div>
     <!-- /.box-body -->
   </div>
   <!-- /.box -->

 </section>
 <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
      $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
    </script>

    <script type="text/javascript">
        @if(session('flash_message'))
 swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
    <script type="text/javascript">
      $('.yyy').click(function(){
        console.log('hello')
        if('button[class=active]'){
          $('#abc').val('T')
        }
        else{
           $('#abc').val('F')
        }
      });
    </script>
@endsection
