@extends('admin.layouts.admin_design')

@section('title')
<title>View visitor - Jane Link Travel</title> 
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <button class="btn  btn-lg btn-info" ><a href="{{route('visitor.create')}}">  Add Visitor</a></button>

        </h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
          <li class="breadcrumb-item"><a href="{{route('visitor.create')}}">Add New</a></li>
          <li class="breadcrumb-item active">View</li>
        </ol>
      </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
         
         <div class="box box-solid box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">visitor Details</h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>S.N</th>
							<th>Visitor Name</th>
              <th>Visitor Contact</th>
              <th>Visitor Address</th>
              <th>Visitor Passport Number</th>

							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
            @foreach($visitor as $visitor_data)           
						<tr>
							<td>{{ $loop->index+1 }}</td>
              <td>{{ $visitor_data->visitor_name }}</td>
              
              <td>{{ $visitor_data->visitor_contact }}</td>
              <td>{{ $visitor_data->visitor_address }}</td>
              <td>{{ $visitor_data->visitor_passport }}</td>
              
							<td>
                <a href="{{ route('visitor.edit',$visitor_data->id) }}" title="Edit" data-toggle="tooltip">
                    <i class="fas fa-edit fa-2x text-info"></i> 
                </a>
                &nbsp;&nbsp;
                <a  href="javascript:" rel="{{$visitor_data->id}}" rel1="visitor/destroy" class="text-inverse deleteRecord" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                    <i class="fas fa-trash-alt fa-2x text-danger"></i>
                </a>
              </td>
            </tr>
            @endforeach
           
					</tbody>
				
				  </table>
				</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
                  
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection



@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
			$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
		}(window, document, jQuery);
    </script>


    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>

@endsection