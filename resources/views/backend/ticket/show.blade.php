@extends('admin.layouts.admin_design')

@section('title')
<title> Ticketing Booking Details - Janelink - </title> 
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     View Ticketing Booking 
   </h1>
   <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
     <li class="breadcrumb-item"><a href="{{route('ticket.index')}}">View All</a></li>
     <li class="breadcrumb-item active">View</li>
   </ol>
 </section>

 <!-- Main content -->
 <section class="content">

  <!-- Basic Forms -->
   <div class="box box-solid box-info">
     <div class="box-header with-border">
       <h6 class="box-subtitle text-white"> Ticketing Booking  Details  </h6>
     </div>
     <!-- /.box-header -->
     <div class="box-body">
       <div class="row">
         <div class="col">
            <div class="row">
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Flight Type</h4>
                    <p>{{ $ticketing ->way_type }}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Book Date</h4>
                    <p>{{ $ticketing ->book_date}}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Type of Service</h4>
                    <p>{{ $ticketing->service->service_name}}</p>
                </div>
            </div>                     
            <hr>
             <div class="row">
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Full Name</h4>
                    <p>{{ $ticketing->full_name }}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Age</h4>
                    <p>{{ $ticketing ->age}}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Gender</h4>
                    <p>{{ $ticketing->gender }}</p>
                </div>
            </div> 

            <div class="row">
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Country</h4>
                    <p>{{ $ticketing->country_id }}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Address</h4>
                    <p>{{ $ticketing ->address}}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Email</h4>
                    <p>{{ $ticketing->email }}</p>
                </div>
            </div> 

            <div class="row">
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Phone Number</h4>
                    <p>{{ $ticketing->phone_number }}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Flight From</h4>
                    <p>{{ $ticketing ->flight_from}}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Flight To</h4>
                    <p>{{ $ticketing->flight_to }}</p>
                </div>
            </div> 
            <div class="row">
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Number of Passengers</h4>
                    <p>{{ $ticketing->number_passenger }}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Number of Kids</h4>
                    <p>{{ $ticketing ->number_kids}}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Arilines</h4>
                    <p>{{ $ticketing->arilines }}</p>
                </div>
            </div> 

             <div class="row">
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Flight Time</h4>
                    <p>{{ $ticketing->flight_time }}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Flight Date</h4>
                    <p>{{ $ticketing ->flight_date}}</p>
                </div>
                               
            </div>

             <div class="row">              
               
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Expenses Cost</h4>
                    <p>{{ $ticketing->expenses_cost}}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Selling Cost</h4>
                    <p>{{ $ticketing->selling_cost}}</p>
                </div>

                 <div class="col-4 col-sm-12 col-md-4">
                    <h4>Additional Cost</h4>
                    <p>{{ $ticketing->additional_cost}}</p>
                </div>
            </div>  

            <div class="row">
                <div class="col-6 col-sm-12 col-md-6">
                    <h4>Total Cost</h4>
                    <p>{{$ticketing->total }}</p>
                </div>
                <div class="col-6 col-sm-12 col-md-6">
                    <h4>Description</h4>
                    <p>{!! $ticketing->description !!}</p>
                </div>
            </div> 
            <div class="row">
                <a href="{{ route('ticket.index') }}">
                    <button class="btn btn-info">Go Back</button>
                </a>
            </div>
                                  
         </div>
       
       </div>
  
     </div>
   </div>

 </section>
</div>
@endsection


@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
			$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
		}(window, document, jQuery);
    </script>

    <script type="text/javascript">
        @if(session('flash_message'))
 swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
@endsection
