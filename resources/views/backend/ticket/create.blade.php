@extends('admin.layouts.admin_design')

@section('title')
<title>Add Ticket Booking - Jane Link Travel</title> 
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     Add New Ticket Booking
   </h1>
   <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
     <li class="breadcrumb-item"><a href="{{route('ticket.index')}}">View All</a></li>
     <li class="breadcrumb-item active">Add ticket</li>
   </ol>
 </section>

 <section class="content">

  <!-- Basic Forms -->
   <div class="box box-solid box-info">
     <div class="box-header with-border">
       <h6 class="box-subtitle text-white"> ticket Details </h6>
     </div>
     <!-- /.box-header -->
     <div class="box-body">
       <div class="row">
         <div class="col">
           <form action="{{route('ticket.store')}}" method="post" enctype="multipart/form-data">
             @csrf

        <div class="form-group">
          <h5>Flight Type:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
                 <div class="demo-radio-button">
                     <input name="way_type" type="radio" v id="radio_1" value="one way" checked />
                     <label for="radio_1">One Way</label>
                     <input name="way_type" type="radio" id="radio_2"  value="two way" />
                     <label for="radio_2">Two Way</label>
                 </div>
         </div>
       </div>
              <div class="form-group">
         <h5>Booking Date:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <?php $dv = Carbon\Carbon::today(); ?>
           <input type="date" name="book_date" id="date" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $dv->toDateString() }}"> </div>
       </div>
       <div class="form-group">
      <h5>Type of Service: &nbsp;<span class="text-danger">*</span></h5>
      <select name="service_id" id="service" class="form-control">                
          <option disabled selected value>Select One Service</option>
          @foreach($service as $service_data)   
          <option value="{{ $service_data->id }}">{{ $service_data->service_name }}</option>
          @endforeach         
      </select>
     </div>
                 <div class="form-group">
                  <h5>Full Name: &nbsp;<span class="text-danger">*</span></h5>
                   <input type="text" name="full_name" id="country_name" class="form-control input-lg" data-validation-required-message=" Title Field is required" />
                   <div id="countryList">
                     </div>
                </div>
           <div class="form-group">
         <h5> Flight Date:&nbsp;<span class="text-danger">*</span></h5>
             <div class="controls">
           <?php $dv = Carbon\Carbon::today(); ?>
           <input type="date" name="flight_date" id="address" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $dv->toDateString() }}"> </div>
             </div> 
              <div class="form-group">
         <h5> Flight Time:&nbsp;<span class="text-danger">*</span></h5>
             <div class="controls">
           <input type="time" name="flight_time" id="time" class="form-control" data-validation-required-message=" Title Field is required" value=""> </div>
             </div>
               <div class="form-group">
                   <h5> Flight From:&nbsp;<span class="text-danger">*</span></h5>
                   </div>
               <div class="row">
                   <div class="col-md-6">
                       <div class="form-group">
                           <h5>Country: &nbsp;<span class="text-danger">*</span></h5>
                           <select name="fromcountryName" id="fromcountryid" class="form-control">
                               <option disabled selected value>Select  Country</option>
                               @foreach($airports as $airport)
                                   <option value="{{ $airport->countryName }}">{{ $airport->countryName }}</option>
                               @endforeach
                           </select>
                       </div>
                   </div>
                   <div class="col-md-6">
                   <div id="airport_list">

                   </div>
                   </div>
               </div>

               <div class="form-group">
                   <h5> Flight To:&nbsp;<span class="text-danger">*</span></h5>
               </div>
               <div class="row">
                   <div class="col-md-6">
                       <div class="form-group">
                           <h5>Country: &nbsp;<span class="text-danger">*</span></h5>
                           <select name="tocountryName" id="tocountryid" class="form-control">
                               <option disabled selected value>Select  Country</option>
                               @foreach($airports as $airport)
                                   <option value="{{ $airport->countryName }}">{{ $airport->countryName }}</option>
                               @endforeach
                           </select>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div id="airport_list_to">

                       </div>
                   </div>
               </div>

               <div class="form-group">
                   <h5> Number of Passengers :&nbsp;<span class="text-danger">*</span></h5>
                   <div class="controls">
                       <input type="number" name="number_passenger" id="number_passengerid" class="form-control" data-validation-required-message=" Title Field is required" value=""> </div>
               </div>

               <div id="passengers_fields"></div>
               <div class="row">
                       <div class="col-sm-3 nopadding">
                           <div class="form-group">
                               <input type="text" class="form-control" id="full_name" name="fullname[]" value="" placeholder="Family/First Middle Name">
                           </div>
                       </div>
                      <div class="col-sm-2 nopadding">
                       <div class="form-group">
                       <div class="input-group">
                       <select class="form-control" id="gender" name="gender[]">
                       <option>Gender</option>
                       <option value="Male">Male</option>
                      <option value="Female">Female</option>
                      <option value="Other">Other</option>
                       </select>
                     </div>
                    </div>
                    </div>
                       <div class="col-sm-1 nopadding">
                           <div class="form-group">
                               <input type="text" class="form-control" id="age" name="age[]" value="" placeholder="Age">
                           </div>
                       </div>
                         <div class="col-sm-2 nopadding">
                           <div class="form-group">
                               <input type="text" class="form-control" id="passportno" name="passportno[]" value="" placeholder="Passport No.">
                           </div>
                       </div>

                         <div class="col-sm-2 nopadding">
                           <div class="form-group">
                               <input type="text" class="form-control" id="phoneno" name="phoneno[]" value="" placeholder="Phone No.">
                           </div>
                       </div>

                       <div class="col-sm-2 nopadding">
                           <div class="form-group">
                               <div class="input-group">
                               <input type="text" class="form-control" id="email" name="email[]" value="" placeholder="Email">
                                  
                                   <div class="input-group-btn">
                                       <button class="btn btn-success" type="button"  onclick="passengers_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
                                   </div>
                               </div>
                           </div>
                       </div>
                       </div>
                       <div class="clear"></div>

        <div class="form-group">
         <h5> Number of Kids (If there are any):&nbsp;<span class="text-danger"></span></h5>
         <div class="controls">
           <input type="number" name="number_kids" id="kids" class="form-control"  value=""> </div>
       </div>

        <div class="form-group">
         <h5> Arilines:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="arilines" id="address" class="form-control" data-validation-required-message=" Title Field is required" value=""> </div>
       </div>

        <div class="form-group">
         <h5>Transaction:&nbsp;<span class="text-danger">*</span></h5>
        
       </div>

       <div id="transaction_fields"></div>
               <div class="row">
                          <div class="col-sm-2 nopadding">
                           <div class="form-group">
                      <input type="text" class="form-control" id="flightnumber" name="flightnumber[]" value="" placeholder="Flight number">
                           </div>
                       </div>
                       <div class="col-sm-2 nopadding">
                           <div class="form-group">
                               <input type="text" class="form-control" id="fromto" name="fromto[]" value="" placeholder="FROM/To">
                           </div>
                       </div>
                     
                       <div class="col-sm-2 nopadding">
                           <div class="form-group">
                               <input type="date" class="form-control" id="flightdate" name="flightdate[]" value="" placeholder="Date">
                           </div>
                       </div>
                         <div class="col-sm-2 nopadding">
                           <div class="form-group">
                               <input type="time" class="form-control" id="timeofdeparture" name="timeofdeparture[]" value="" placeholder="Time of departure">
                           </div>
                       </div>

                         <div class="col-sm-2 nopadding">
                           <div class="form-group">
                               <input type="time" class="form-control" id="arrivaltime" name="arrivaltime[]" value="" placeholder="Arrival time">
                           </div>
                       </div>

                       <div class="col-sm-2 nopadding">
                           <div class="form-group">
                               <div class="input-group">
                              <input type="text" class="form-control" id="sts" name="sts[]" value="" placeholder="STS">
                                  
                                   <div class="input-group-btn">
                                       <button class="btn btn-success" type="button"  onclick="transaction_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
                                   </div>
                               </div>
                           </div>
                       </div>
                       </div>
                       <div class="clear"></div>
       
       <div class="form-group">
         <h5> Expenses Cost :&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="expenses_cost" id="price" class="form-control" data-validation-required-message=" Title Field is required" value=""> </div>
       </div>

       <div class="form-group">
         <h5> Selling Cost :&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="selling_cost" id="selling_costid" class="form-control" data-validation-required-message=" Title Field is required" value=""> </div>
       </div>

       <div class="form-group">
        <h5> Additional Cost :&nbsp;<span class="text-danger"></span></h5>
        <div class="controls">
          <input type="number" name="additional_cost" id="additional_cost" class="form-control"  value=""> </div>
      </div>
      <div class="form-group">
      <h5>Person in charge: &nbsp;<span class="text-danger">*</span></h5>
      <select name="employee_id" id="employee" class="form-control">                
          <option disabled selected value>Select One Person</option>
          @foreach($employee as $employee_data)   
          <option value="{{ $employee_data->id }}">{{ $employee_data->employee_name }}</option>
          @endforeach         
      </select>
     </div>
        <div class="form-group">
          <h5>Description:</h5>
          <div class="controls">
              <textarea id="editor1" name="description" rows="10" cols="80">

              </textarea>
        </div>
       </div>

          
        <br>
        <div class="text-xs-right text-center">
            <button type="submit" class="btn btn-info btn-lg">Submit</button>
          </div>  
     </form>

         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </div>
     <!-- /.box-body -->
   </div>
   <!-- /.box -->

 </section>
 <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
			$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
		}(window, document, jQuery);
    </script>

    <script type="text/javascript">
        @if(session('flash_message'))
 swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){

        $('#country_name').keyup(function(){
            var query = $(this).val();

            if(query != '')
            {
                var _token = $('input[name="_token"]').val();
               // console.log(country_name)
                $.ajax({
                    url:"{{ route('autocomplete.fetch') }}",
                    method:"POST",
                    data:{query:query, _token:_token},

                    success:function(data){
                        $('#countryList').fadeIn();
                        $('#countryList').html(data);
                    }
                });
            }
        });

        $(document).on('click', 'li', function(){
            $('#country_name').val($(this).text());
            $('#countryList').fadeOut();
        });

    });
</script>
<script>
    var room = 1;

    function passengers_fields() {

        room++;
        var objTo = document.getElementById('passengers_fields')
        var divtest = document.createElement("div");
        divtest.setAttribute("class", "form-group removeclass"+room);
        var rdiv = 'removeclass'+room;
        divtest.innerHTML = '<div class="row"><div class="col-sm-3 nopadding"><div class="form-group"><input type="text" class="form-control" id="fullname" name="fullname[]" value="" placeholder="Family/First Middle Name"></div></div><div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"><select class="form-control" id="gender" name="gender[]"><option>Gender</option><option value="Male">Male</option><option value="Female">Female</option><option value="Other">Other</option></select></div></div></div><div class="col-sm-1 nopadding"><div class="form-group"><input type="text" class="form-control" id="age" name="age[]" value="" placeholder="Age"></div></div><div class="col-sm-2 nopadding"><div class="form-group"><input type="text" class="form-control" id="passportno" name="passportno[]" value="" placeholder="Passport No."></div></div><div class="col-sm-2 nopadding"><div class="form-group"><input type="text" class="form-control" id="phoneno" name="phoneno[]" value="" placeholder="Phone No."></div></div><div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"><input type="text" class="form-control" id="email" name="email[]" value="" placeholder="Email"><div class="input-group-btn"> <button class="btn btn-danger" type="button" onclick="remove_passengers_fields('+ room +');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div><div class="clear"></div></div>';

        objTo.appendChild(divtest)
    }
    function remove_passengers_fields(rid) {
        $('.removeclass'+rid).remove();
    }</script>


    <script>
    var room = 1;
    function transaction_fields() {

        room++;
        var objTo = document.getElementById('transaction_fields')
        var divtest = document.createElement("div");
        divtest.setAttribute("class", "form-group removeclass"+room);
        var rdiv = 'removeclass'+room;
        divtest.innerHTML = '<div class="row"><div class="col-sm-2 nopadding"><div class="form-group"><input type="text" class="form-control" id="flightnumber" name="flightnumber[]" value="" placeholder="Flight number"></div></div><div class="col-sm-2 nopadding"><div class="form-group"><input type="text" class="form-control" id="fromto" name="fromto[]" value="" placeholder="FROM/To"></div></div><div class="col-sm-2 nopadding"><div class="form-group"><input type="date" class="form-control" id="flightdate" name="flightdate[]" value="" placeholder="Date"></div></div><div class="col-sm-2 nopadding"><div class="form-group"><input type="time" class="form-control" id="timeofdeparture" name="timeofdeparture[]" value="" placeholder="Time of departure"></div></div><div class="col-sm-2 nopadding"><div class="form-group"><input type="time" class="form-control" id="arrivaltime" name="arrivaltime[]" value="" placeholder="Arrival time"></div> </div><div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"><input type="text" class="form-control" id="sts" name="sts[]" value="" placeholder="STS"><div class="input-group-btn"> <button class="btn btn-danger" type="button" onclick="remove_transaction_fields('+ room +');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div></div>';

        objTo.appendChild(divtest)
    }
    function remove_transaction_fields(rid) {
        $('.removeclass'+rid).remove();
    }</script>


<script type="text/javascript">
    $(document).ready(function(){
        $('#fromcountryid').change(function(){
            var fromcountryid = $(this).val();
            console.log(fromcountryid)
            $.ajax({
                type:'get',
                url:'{!!URL::to('findcountry')!!}',
                data:{'fromcountryid':fromcountryid},
                success: function(data){
                    console.log(data);
                    // for(var i=0;i<data.length;i++)
                    // {
                        // console.log(data[i].fromcountryid);
                        $('#airport_list').html(data);
                    // }
                }
            });
        })
    })
</script>


<script type="text/javascript">
    $(document).ready(function(){
        $('#tocountryid').change(function(){
            var tocountryid = $(this).val();
           // console.log(tocountryid)
            $.ajax({
                type:'get',
                url:'{!!URL::to('findcountryto')!!}',
                data:{'tocountryid':tocountryid},
                success: function(data){
                   // console.log(data);
                    // for(var i=0;i<data.length;i++)
                    // {
                    // console.log(data[i].fromcountryid);
                    $('#airport_list_to').html(data);
                    // }
                }
            });
        })
    })
</script>


@endsection
