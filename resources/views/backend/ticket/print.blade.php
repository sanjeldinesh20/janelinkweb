@extends('admin.layouts.admin_design')

@section('title')
    <title> Ticketing Booking Details - Janelink - </title>
@endsection

@section('content')
    <style>
        table, td, th {
            border: 1px solid black;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th {
            height: 50px;
        }
    </style>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Invoice
                {{--<small>Bill NO: 0154879</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
                <li class="breadcrumb-item"><a href="#">{{$ticketed->full_name}}</a></li>
                <li class="breadcrumb-item active">Invoice</li>
            </ol>
        </section>



        <!-- Main content -->
        <div id="printpage">
            <section class="invoice printableArea">
                <div class="col-12">
                    <div class="page-header" style="text-align: center;">
                        <h2 class="d-inline" style="text-align: center;color:#FCC114; "><span class="font-size-30">JaneLink Travel Pvt. Ltd.</span></h2><br>
                        <h3 class="d-inline" style="text-align: center; margin-left:50px; color:#FCC114;" ><span class="font-size-20">請求書/予約確認書</span></h3>
                    </div>
                </div>
                <div class="row">
                    <img src="{{asset('public/uploads/setting/small/'.$sites_data->organization_logo)}}" alt="{{$sites_data->organization_name}}" width="260px">

                    <div class="text" style="text-align: right;">
                        <strong class="text-blue font-size-16">{{$sites_data->organization_name}}</strong><br>
                        {{$sites_data->organization_address}}<br>
                        <strong>Phone: {{$sites_data->contact_number}}&nbsp</strong>
                        <strong>Email: {{$sites_data->organization_email}}</strong>
                    </div>
                </div>



                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-bordered test">

                            <tr style="color: royalblue;">
                                <th>予約 No.</th>
                                <th>PHL688</th>
                                <th>人数</th>
                                <th>{{$ticketed->number_passenger}} 名</th>

                            </tr>
                            <tr style="background-color: #ffffaa;">
                                <td>方面</td>
                                <td>アジア</td>
                                <td>販売店名</td>
                                <td>ジャネリンク トラベル</td>
                            </tr>

                            <tr style="background-color: #ffffaa">
                                <td>日本出発日</td>
                                <td>{{$ticketed->flight_date}}</td>
                                <td>担当者様名</td>
                                <td>{{$ticketed->employee->employee_name}}</td>
                            </tr>



                        </table>
                    </div>
                    <!-- /.col -->
                </div>

                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-bordered">

                            <tr style="color: royalblue;">

                                <th>名前（Family/First Middle）</th>
                                <th>性別</th>
                                <th>年齢</th>
                                <th>ご利用の航空会社</th>

                            </tr>
                            <tr style="background-color: #ffffaa">

                                <td>
                                    <?php $details = json_decode($ticketed->fullname); ?>
                                    <ul>
                                        @for($i=0;$i< sizeof($details[0]);$i++)
                                            <li>{{ ucfirst($details[0][$i] )}}</li>
                                        @endfor
                                    </ul>
                                </td>
                                <td>
                                    <?php $details = json_decode($ticketed->gender); ?>
                                    <ul>
                                        @for($i=0;$i< sizeof($details[0]);$i++)
                                            <li>{{ ucfirst($details[0][$i] )}}</li>
                                        @endfor
                                    </ul>
                                </td>
                                <td>
                                    <?php $details = json_decode($ticketed->age); ?>
                                    <ul>
                                        @for($i=0;$i< sizeof($details[0]);$i++)
                                            <li>{{ ucfirst($details[0][$i] )}}</li>
                                        @endfor
                                    </ul>
                                </td>
                                <td>{{$ticketed->arilines}}</td>
                            </tr>


                        </table>
                    </div>
                    <!-- /.col -->
                </div>

                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-bordered">

                            <tr style="color: royalblue;">
                                <th>便名</th>
                                <th>FROM/TO</th>
                                <th>日付</th>
                                <th>発時刻</th>
                                <th>着時刻</th>
                                <th>STS</th>

                            </tr>
                            <tr style="background-color: #ffffaa">
                                <td>
                                    <?php $details = json_decode($ticketed->flightnumber); ?>
                                    <ul>
                                        @for($i=0;$i< sizeof($details[0]);$i++)
                                            <li>{{ ucfirst($details[0][$i] )}}</li>
                                        @endfor
                                    </ul>
                                </td>
                                <td>
                                    <?php $details = json_decode($ticketed->fromto); ?>
                                    <ul>
                                        @for($i=0;$i< sizeof($details[0]);$i++)
                                            <li>{{ ucfirst($details[0][$i] )}}</li>
                                        @endfor
                                    </ul>
                                </td>
                                <td>
                                    <?php $details = json_decode($ticketed->flightdate); ?>
                                    <ul>
                                        @for($i=0;$i< sizeof($details[0]);$i++)
                                            <li>{{ ucfirst($details[0][$i] )}}</li>
                                        @endfor
                                    </ul>
                                </td>
                                <td>
                                    <?php $details = json_decode($ticketed->timeofdeparture); ?>
                                    <ul>
                                        @for($i=0;$i< sizeof($details[0]);$i++)
                                            <li>{{ ucfirst($details[0][$i] )}}</li>
                                        @endfor
                                    </ul>
                                </td>
                                <td>
                                    <?php $details = json_decode($ticketed->arrivaltime); ?>
                                    <ul>
                                        @for($i=0;$i< sizeof($details[0]);$i++)
                                            <li>{{ ucfirst($details[0][$i] )}}</li>
                                        @endfor
                                    </ul>
                                </td>
                                <td>
                                    <?php $details = json_decode($ticketed->sts); ?>
                                    <ul>
                                        @for($i=0;$i< sizeof($details[0]);$i++)
                                            <li>{{ ucfirst($details[0][$i] )}}</li>
                                        @endfor
                                    </ul>
                                </td>
                            </tr>

                        </table>
                    </div>
                    <!-- /.col -->
                </div>

                <h4 style="color:#red;">CHECK IN AT AIRPORT : {{$ticketed->flight_date}}
                   , {{$ticketed->flight_time}}</h4>
                <p>AIRFARE: {{$ticketed->selling_cost}} YEN /ADULT          TOTAL AMOUNT: {{$ticketed->total}} YEN <br>
                    FULL PAYMENT UNTILL: {!! $ticketed->created_at->addDays(7)->format('y-m-d') !!}
                </p>
                <h5>CANCEL CHARGE ( BEFORE DEPARTURE) :￥30,000 (AFTER DEPARURE):100% OF THE TICKET PRICE
                    FIX OPEN/FIX TICKET: 3 MONTHS FIX OPEN (RETURN DATE CHANGE PERMITTED WITHIN 3 MONTHS
                    FROM THE DEPARTURE DATE WITH THE CHARGE OF 7,000 YEN. THE CHARGE MIGHT BE DIFFERENT,
                    DEPENDING UPON SEAT AVAILABILITY)</h5>
                <br>
                <p>*Please note that, the handling fee for the flight reservation, or fees for other arrangements are NON-REFUNDABLE.
                    <br>
                    *Due to the circumstances of the airline, airfare (including Tax and Fuel surcharge) may change before your ticket
                    <br>
                    is issued without advance notice and if there is an increase, you are required to pay the balance. <br>
                    *In case of changes or cancellations made by the client, the required charges will be deducted from the value to
                    <br>
                    be returned (in case of refund) <br>
                    For more information about refund, please contact us directly. <br>
                    *For further questions/information, please contact JANE LINK TRAVEL OFFICE.</p>

                <div class="test">
                    BANK ACCOUNT INFORMATION:
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <th>ゆうちょ 銀行 ( JAPAN POST BANK )</th>
                                    <th> 三菱東京UFJ銀行 ( MITSUBISHI TOKYO UFJ BANK ) </th>


                                </tr>
                                <tr>
                                    <td>10310-91398081  </td>
                                    <td>    店番364　口座番号 ‎0293166 (普通預金)</td>

                                </tr>
                                <tr>
                                    <td>口座名: カ）ジャネ リンク</td>
                                    <td>口座名：カブシキガイシヤ　ジヤネ　リンク</td>

                                </tr>
                                <tr>
                                    <td>ゼロサンハチ店</td>
                                    <td>支店名：大久保支店</td>

                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>

                </div>

                <div class="row no-print plink" >
                    <div class="col-xs-12">
                        <a href="" class="btn btn-primary" onclick="printdiv()"><i class="fa fa-print"></i> Print</a>
                        <div class = "row col-md-3 pull-right">
                            <a href="{{ route('ticket.index') }}" class="btn btn-primary pull-right">Cancel</a>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
@endsection


@section('scripts')

    <script type="text/javascript">
        function printdiv()
        {
            //your print div data
            //alert(document.getElementById("printpage").innerHTML);
            $('.plink').hide();
            var newstr=document.getElementById("printpage").innerHTML;

            var header='<link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"><link href="{{ asset('fonts/font-awesome.min.css') }}" rel="stylesheet"><link href="{{ asset('print/print.css') }}" rel="stylesheet">'

            var footer =''
            var popupWin = window.open('', '_blank', 'width=1100,height=600');
            popupWin.document.open();
            popupWin.document.write('<html>'+header+'<body onload="window.print()">'+ newstr + '</html>');
            popupWin.document.close();
            return false;
        }
    </script>
    <!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
        ! function(window, document, $) {
            "use strict";
            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        }(window, document, jQuery);
    </script>

    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
@endsection
