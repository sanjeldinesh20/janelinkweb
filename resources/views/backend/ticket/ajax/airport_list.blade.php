<div class="col-md-6">
    <div class="form-group">
        <h5>Airport: &nbsp;<span class="text-danger">*</span></h5>
        <select name="fromairportName" id="tocountryid" class="form-control">
            <option disabled selected value>Select  Airport</option>
            @foreach($airports as $airport)
                <option value="{{ $airport->name }}">{{ $airport->name }}</option>
            @endforeach
        </select>
    </div>
</div>



