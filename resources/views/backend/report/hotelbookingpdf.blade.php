<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{$sites_data->organization_name}} Report</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        .pdf{
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .heading {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
            text-align: center;

        }

        .details{
            background: #ffffaa;

            text-align: center;
        }

        .total {
            border-top: 2px solid #eee;
            font-weight: bold;
            text-align: right;
            margin-right: 30px;
        }
    .total  td{
        border: 1px solid black;
    }

        .heading th{
            padding:10px;
            border: 1px solid black;
        }
        .details td{
            padding: 10px;
            border: 1px solid black;
        }
    </style>
</head>

<body>
<div class="pdf">
    <div class="test" style="text-align: center;">
        <img src="{{asset('public/uploads/setting/small/'.$sites_data->organization_logo)}}">
        <h1 style="color:#0D8BBD" >{{$sites_data->organization_name}} Hotel Booking Report</h1>
        <p style="color: #0D8BBD">{{$sites_data->organization_address}} &nbsp;{{$sites_data->contact_number}} &nbsp;
        {{$sites_data->organization_email}}</p>

    </div>
    <br>
    <table style="width: 100%;">
        <tr class="heading">
            <th>S.N.</th>
            <th>Date</th>
            <th>Expense BY</th>
            <th>Expense Price</th>
            <th>Selling Price</th>
            <th>Additional Price</th>
            <th>Total Price</th>
        </tr>
        @foreach($hotel as $hote)
        <tr class="details">
            <td>{{$loop->index+1}}</td>
            <td>{{$hote->date}}</td>
            <td>{{$hote->names}}</td>
            <td>{{$hote->total_expense}}</td>
            <td>{{$hote->total_selling}}</td>
            <td>{{$hote->total_additional}}</td>
            <td>40</td>
        </tr>
        @endforeach

    </table>
   <div class="total">
       Total Amount :12000
   </div>
</div>
</body>
</html>