<table id="example"
       class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
    <thead>
    <tr>
        <th>S.N.</th>
        <th>Date</th>
        <th>Expenses Title</th>
        <th>employee</th>
        <th>Total Ex</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $expense)
        <tr>
            <td>{{ $loop->index+1 }}</td>
            <td>{{ $expense->expense_date}}</td>
            <td>{{ $expense->expense_title}}</td>
            <td>{{ $expense->expense_price}}</td>
            <td>{{ $expense->employee_id}}</td>
            <td>{{ $total_expense+=$expense->expense_price}}</td>
        </tr>
    @endforeach
    </tbody>
</table>