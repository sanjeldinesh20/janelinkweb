<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>A simple, clean, and responsive HTML invoice template</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        .pdf{
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .heading {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
            text-align: center;

        }

        .details{
            background: #ffffaa;

            text-align: center;
        }

        .total {
            border-top: 2px solid #eee;
            font-weight: bold;
            text-align: center;
        }


        .heading th{
            padding:10px;
        }
        .details td{
            padding: 10px;
        }
    </style>
</head>

<body>
<div class="pdf">
    <div class="test" style="text-align: center;">
        <img src="{{asset('public/uploads/setting/small/'.$sites_data->organization_logo)}}">
        <h1 >{{$sites_data->organization_name}}</h1>
        {{$sites_data->organization_address}} &nbsp;{{$sites_data->contact_number}} &nbsp;
        {{$sites_data->organization_email}}
    </div>
    <br>
    <table style="width: 100%;">
        <tr class="heading">
            <th>S.N.</th>
            <th>Expense Date</th>
            <th>Expense Title</th>
            <th>Expense Total</th>
        </tr>
        <tr class="details">
            <td>1</td>
            <td>2050-2-6</td>

            <td>Title</td>
            <td>2050.22</td>

        </tr>
        <tr class="details">
            <td>2</td>
            <td>2050-2-6</td>

            <td>Title</td>
            <td>2050.25</td>

        </tr>
        <tr class="details">
            <td>3</td>
            <td>2050-2-6</td>

            <td>Title</td>
            <td>2050.22</td>

        </tr>
        <tr class="total">
            <td></td><td></td><td></td>

            <td>
                Total: 10000.00
            </td>
        </tr>
    </table>
</div>
</body>
</html>