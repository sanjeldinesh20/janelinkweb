@extends('admin.layouts.admin_design')

@section('title')
    <title>View Report - Jane Link Travel</title>
@endsection

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                View Report
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
                <li class="breadcrumb-item"><a href="{{route('expense.index')}}">Add New Expenses</a></li>
                <li class="breadcrumb-item active">View Report</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                    <a href="{{route('report.generate')}}">
                        <button class="btn btn-primary">Generate Report</button>
                    </a>
                        &nbsp;&nbsp;
                    <div class="button dropdown">
                        <select id="colorselector">
                            <option>Select One</option>
                            <option value="day">Daily</option>
                            <option value="month">Monthly</option>
                            <option value="year">Yearly</option>
                        </select>
                    </div>
                    </div>
                    <br>
                    <div class="output">
                        <div id="day" class="colors day" style="display: none">
                            <div class="input-group ">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="date" class="form-control pull-right datereport" id="datepicker">
                            </div>
                        </div>
                        <div id="month" class="colors month" style="display: none">
                            <div class="input-group ">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="date" class="form-control pull-right datereport" id="datepickermonth">
                            </div>
                        </div>
                        <div id="year" class="colors year" style="display: none">
                            <div class="input-group ">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="date" class="form-control pull-right datereport " id="datepickeryear">
                            </div>
                        </div>
                    </div>

                    <div class="box box-solid box-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title">Hover Export Data Table</h4>
                            <h6 class="box-subtitle text-white-50">Export data to Copy, CSV, Excel, PDF & Print</h6>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example"
                                       class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                    <thead>
                                    <tr>
                                        <th>S.N.</th>
                                        <th>Date</th>
                                        <th>Expenses Title</th>
                                        <th>Expenses Price</th>
                                        <th>Employee</th>
                                        <th>Total Ex</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>1</td>

                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->

                    </div>

                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>


    </div>
    <!-- /.content-wrapper -->

@endsection



@section('scripts')
    <!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
        !function (window, document, $) {
            "use strict";
            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        }(window, document, jQuery);
    </script>



    <script type="text/javascript">
        $(function () {
            $("#datepicker").datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy/mm/dd',
            })
                .datepicker('update', new Date());

        });

        $(function () {
            $('#datepickermonth').datepicker({
                viewMode: 'years',
                format: 'mm-yyyy',
                minViewMode: "months",
                autoclose: true,
                todayHighlight: true,
            });

        });

        $(function () {
            $('#datepickeryear').datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years",


            });

        });

    </script>

    <script>
        $(function() {
            $('#colorselector').change(function(){
                $('.colors').hide();
                $('#' + $(this).val()).show();
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#colorselector').change(function(){
                var date_filter = $(this).val();
                console.log(date_filter)
                $.ajax({
                    type:'get',
                    url:'{!!URL::to('filterexpe')!!}',
                    data:{'date_filter':date_filter,date:$('.datereport').val()},
                    success: function(data){
                        console.log(data);
                        $('#example').html(data);
                    }
                });
            })
        })


    </script>



@endsection