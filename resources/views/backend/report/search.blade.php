@extends('admin.layouts.admin_design')

@section('title')
<title>View Result - Jane Link Travel</title> 
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          View Report
        </h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
          <li class="breadcrumb-item"><a href="{{route('expense.index')}}">Add New Expenses</a></li>
          <li class="breadcrumb-item active">View Result</li>
        </ol>
      </section>



      @if($search == 0)
          <h3 style="text-align: center;">View Report </h3>
      <form action="{{route('day.report')}}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="row">
              <div class="col-6 col-md-6">

                  <h5>Report From:&nbsp;<span class="text-danger">*</span></h5>
          <div class="input-group date">
              <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
              </div>
              <input type="text" name="firstdate" class="form-control pull-right" id="datepicker">
          </div>

              </div>
              <div class="col-6 col-md-6">

                  <h5>Report From:&nbsp;<span class="text-danger">*</span></h5>
                  <div class="input-group date">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="lastdate" class="form-control pull-right" id="datepicker1">
                  </div>

              </div>
          </div>
          <br>
          <div class="text-xs-right text-center">
              <button type="submit" class="btn btn-info btn-lg">Submit</button>
          </div>
      </form>
      @endif
      @if($search == 1)
          <h3 style="text-align: center;">View Report </h3>
          <form action="{{route('day.report')}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="row">
                  <div class="col-6 col-md-6">

                      <h5>Report From:&nbsp;<span class="text-danger">*</span></h5>
                      <div class="input-group date">
                          <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" name="firstdate" class="form-control pull-right" id="datepicker" value="{{$start}}">
                      </div>

                  </div>
                  <div class="col-6 col-md-6">

                      <h5>Report From:&nbsp;<span class="text-danger">*</span></h5>
                      <div class="input-group date">
                          <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" name="lastdate" class="form-control pull-right" id="datepicker1" value="{{$end}}">
                      </div>

                  </div>
              </div>
              <br>
              <div class="text-xs-right text-center">
                  <button type="submit" class="btn btn-info btn-lg">Submit</button>
              </div>
          </form>
       <section class="content">
      <div class="row">
        <div class="col-12">

         <div class="box box-solid box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">View Report </h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
        <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>S.N</th>
              <th>Date</th>
              <th> Title</th>
                <th>Expenses By</th>
                <th>Expenses Price</th>
            </tr>
          </thead>
          <tbody>
          <?php $total=0?>
            @foreach($expenses as $expense)
            <tr>
              <td>{{ $loop->index+1 }}</td>
              <td>{{ $expense->expense_date}}</td>
              <td> {{ $expense->expense_title }}</td>
                <td> {{ $expense->employee->employee_name }}</td>
                <td> {{ $expense->expense_price }}</td>
                <?php $total+= $expense->expense_price
                ?>

            </tr>
            @endforeach

          </tbody>

          </table>

        </div>
              <h3 style="text-align: center"> Total Expenses In {{$start}} to {{$end}} = <span style="color: red">{{$total}}</span> </h3>

            </div>
          </div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
      @endif
</div>


@endsection



@section('scripts')

    <script type="text/javascript">
        $(function () {
            $("#datepicker").datepicker({

                todayHighlight: true,
                format: 'yyyy/mm/dd',
            })

        });
        $(function () {
            $("#datepicker1").datepicker({

                todayHighlight: true,
                format: 'yyyy/mm/dd',
            })

        });
    </script>

@endsection