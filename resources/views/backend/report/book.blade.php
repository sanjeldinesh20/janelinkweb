@extends('admin.layouts.admin_design')

@section('title')
    <title>View Report - Jane Link Travel</title>
@endsection

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                View Report
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
                <li class="breadcrumb-item"><a href="{{route('expense.index')}}">Add New Expenses</a></li>
                <li class="breadcrumb-item active">View Report</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <a href="{{route('report.generate')}}">
                        <button class="btn btn-primary">Generate Report</button>
                    </a>

                    <select name="date_filter" id="date-filter">
                        <option value="daily">Daily</option>
                        <option value="monthly">Monthly</option>
                        <option value="yearly">Yearly</option>
                    </select>
                    <br>    <br>
                    <div class="box box-solid box-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title">Hover Export Data Table</h4>
                            <h6 class="box-subtitle text-white-50">Export data to Copy, CSV, Excel, PDF & Print</h6>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example"
                                       class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                    <thead>
                                    <tr>
                                        <th>S.N.</th>
                                        <th>Date</th>
                                        <th>Book by Title</th>
                                        <th>Total Expenses Cost</th>
                                        <th>Total Selling Cost</th>
                                        <th>Total Addition Cost</th>

                                        <th>Total Ex</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($hotel as $expense)
                                        <tr>
                                            <td>1</td>
                                            <td>{{ $expense->book_date}}</td>
                                            <td>{{ $expense->full_name}}</td>
                                            <td>{{ $expense->total_expense}}</td>
                                            <td>{{ $expense->total_selling}}</td>
                                            <td>{{ $expense->total_additional}}</td>

                                            <td>dds</td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>


    </div>


@endsection
