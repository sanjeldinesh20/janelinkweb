@extends('admin.layouts.admin_design')

@section('title')
    <title> Ticketing Booking Details - Janelink - </title>
@endsection

@section('content')
    <style>
        table, td, th {
            border: 1px solid black;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th {
            height: 50px;
        }
    </style>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
               Salary  Invoice
                {{--<small>Bill NO: 0154879</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
                <li class="breadcrumb-item"><a href="#">{{ $salary->employee->employee_name }}</a></li>
                <li class="breadcrumb-item active">Invoice</li>
            </ol>
        </section>



        <!-- Main content -->
        <div id="printpage">
            <section class="invoice printableArea">
                <div class="col-12">
                    <div class="page-header" style="text-align: center;">
                        <h2 class="d-inline" style="text-align: center;color:#FCC114; "><span class="font-size-30">JaneLink Travel Pvt. Ltd.</span></h2><br>
                        <h3 class="d-inline" style="text-align: center; margin-left:50px; color:#FCC114;" ><span class="font-size-20">請求書/予約確認書</span></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col0-md-6">
                    <img src="{{asset('public/uploads/setting/small/'.$sites_data->organization_logo)}}" alt="{{$sites_data->organization_name}}" width="260px">
                    </div>
                    <div class="col-md-6">
                    <div class="text" style="text-align: right;">
                        <strong class="text-blue font-size-16">{{$sites_data->organization_name}}</strong><br>
                        {{$sites_data->organization_address}}<br>
                        <strong>Phone: {{$sites_data->contact_number}}&nbsp</strong>
                        <strong>Email: {{$sites_data->organization_email}}</strong>
                    </div>
                </div>
                </div>



                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-bordered test">

                            <tr style="color: royalblue;">
                                <th>Name:</th>
                                <th>Month </th>
                                <th>Basic Salary </th>
                                <th>Advance</th>
                                <th>Total</th>

                            </tr>
                            <tr style="background-color: #ffffaa;">
                                <td>{{$salary->employee->employee_name}}</td>
                                <td>{{$salary->month->monthname}}</td>
                                <td>{{$salary->basic_salary}}</td>
                                <td>{{$salary->advance_id}}</td>
                                <td>{{$salary->total}}</td>
                            </tr>





                        </table>
                    </div>
                    <!-- /.col -->
                </div>



                <div class="row no-print plink" >
                    <div class="col-xs-12">
                        <a href="" class="btn btn-primary" onclick="printdiv()"><i class="fa fa-print"></i> Print</a>
                        <div class = "row col-md-3 pull-right">
                            <a href="{{ route('salary.index') }}" class="btn btn-primary pull-right">Cancel</a>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
@endsection


@section('scripts')

    <script type="text/javascript">
        function printdiv()
        {
            //your print div data
            //alert(document.getElementById("printpage").innerHTML);
            $('.plink').hide();
            var newstr=document.getElementById("printpage").innerHTML;

            var header='<link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"><link href="{{ asset('fonts/font-awesome.min.css') }}" rel="stylesheet"><link href="{{ asset('print/print.css') }}" rel="stylesheet">'

            var footer =''
            var popupWin = window.open('', '_blank', 'width=1100,height=600');
            popupWin.document.open();
            popupWin.document.write('<html>'+header+'<body onload="window.print()">'+ newstr + '</html>');
            popupWin.document.close();
            return false;
        }
    </script>
    <!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
        ! function(window, document, $) {
            "use strict";
            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        }(window, document, jQuery);
    </script>

    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
@endsection
