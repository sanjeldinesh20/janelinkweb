@extends('admin.layouts.admin_design')

@section('title')
<title>Edit salary - Jane Link Travel</title> 
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     Edit salary
   </h1>
   <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
     <li class="breadcrumb-item"><a href="{{route('salary.index')}}">View All</a></li>
     <li class="breadcrumb-item active">Edit salary</li>
   </ol>
 </section>

 <section class="content">

  <!-- Basic Forms -->
   <div class="box box-solid box-info">
     <div class="box-header with-border">
       <h6 class="box-subtitle text-white"> salary Details </h6>
     </div>
     <!-- /.box-header -->
     <div class="box-body">
       <div class="row">
         <div class="col">
             <form action="{{route('salary.update',$salary->id)}}" method="post" enctype="multipart/form-data">
             @csrf
        <div class="form-group">
            <h5>Select Employee  Name: &nbsp;<span class="text-danger">*</span></h5>
            <select name="employee_id" id="id" class="form-control"> 
               @foreach($employee as $emplo) 
                <option value="{{ $emplo->id}}">{{ $emplo->employee_name}}</option>
                 @endforeach
            </select>
        </div>

       <div class="form-group">
            <h5>Select Month: &nbsp;<span class="text-danger">*</span></h5>
            <select name="month_id" id="month" class="form-control">  
               @foreach($months as $month)              
                <option value="{{ $month->id}}">{{ $month->monthname}}</option>
              @endforeach
            </select>
      </div>

       <div class="form-group">
         <h5>Basic Salary:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="basic_salary" id="basicid" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $salary->basic_salary }}"> </div>
       </div>

         <div class="form-group">
         <h5>Advanced:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="advance_id" id="advanceid" class="form-control" data-validation-required-message=" Title Field is required" value=""> </div>
        </div>

        <div class="form-group">
         <h5>Total:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="total" id="total" class="form-control" data-validation-required-message=" Title Field is required" value=""> </div>
       </div>

       
        <div class="form-group">
          <h5>Description:</h5>
          <div class="controls">
              <textarea id="editor1" name="description" rows="10" cols="80">
{{ $salary->description }}
              </textarea>
                           
        </div>
       </div>

          
        <br>
        <div class="text-xs-right text-center">
            <button type="submit" class="btn btn-info btn-lg">Submit</button>
          </div>  
     </form>


         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </div>
     <!-- /.box-body -->
   </div>
   <!-- /.box -->

 </section>
 <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
      $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
    </script>

    <script type="text/javascript">
        @if(session('flash_message'))
 swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>


<script type="text/javascript">
    $(document).ready(function(){
        $('#employee_id').change(function(){
            var employee_id = $(this).val();
            console.log(employee_id)
            $.ajax({
                type:'get',
                url:'{!!URL::to('findadvance')!!}',
                data:{'employee_id':employee_id},
                success: function(data){
                    console.log(data);
                    for(var i=0;i<data.length;i++)
                    {
                        console.log(data[i].employee_id);
                        $('#advanceid').val(data[i].advance_amount);
                    }
                }
            });
        })
    })


</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#employee_id').change(function(){
            var employee_id = $(this).val();
            console.log(employee_id)
            $.ajax({
                type:'get',
                url:'{!!URL::to('findbasic')!!}',
                data:{'employee_id':employee_id},
                success: function(data){
                    console.log(data);
                    for(var i=0;i<data.length;i++)
                    {
                        console.log(data[i].employee_id);
                        $('#basicid').val(data[i].basic_amount);
                    }
                }
            });
        })
    })


</script>
@endsection
