@extends('admin.layouts.admin_design')

@section('title')
<title> Hotel Booking Details - Janelink - </title> 
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     View hotel Booking 
   </h1>
   <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
     <li class="breadcrumb-item"><a href="{{route('hotel.index')}}">View All</a></li>
     <li class="breadcrumb-item active">View</li>
   </ol>
 </section>

 <!-- Main content -->
 <section class="content">

  <!-- Basic Forms -->
   <div class="box box-solid box-info">
     <div class="box-header with-border">
       <h6 class="box-subtitle text-white"> Hotel Booking  Details</h6>
     </div>
     <!-- /.box-header -->
     <div class="box-body">
       <div class="row">
         <div class="col">
            <div class="row">
                <div class="col-6 col-sm-12 col-md-6">
                    <h4>Date</h4>
                    <p>{{ $hotel ->book_date }}</p>
                </div>
                <div class="col-6 col-sm-12 col-md-6">
                    <h4>Service Name</h4>
                    <p>{{ $hotel->service->service_name}}</p>
                </div>
            </div>                     
            <hr>
             <div class="row">
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Full Name</h4>
                    <p>{{ $hotel->full_name }}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Age</h4>
                    <p>{{ $hotel ->age}}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Gender</h4>
                    <p>{{ $hotel->gender }}</p>
                </div>
            </div> 

            <div class="row">
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Country</h4>
                    <p>{{ $hotel->country->name }}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Address</h4>
                    <p>{{ $hotel ->address}}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Email</h4>
                    <p>{{ $hotel->email }}</p>
                </div>
            </div> 

            <div class="row">
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Phone Number</h4>
                    <p>{{ $hotel->phone_number }}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Arrival - Date</h4>
                    <p>{{ $hotel ->arrival_date}}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Departure - Date</h4>
                    <p>{{ $hotel->departure_date }}</p>
                </div>
            </div> 
            <div class="row">
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Number of Adults</h4>
                    <p>{{ $hotel->number_adults }}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Number of Kids</h4>
                    <p>{{ $hotel ->number_kids}}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Number of Nights at Hotel</h4>
                    <p>{{ $hotel->nights_at_hotel }}</p>
                </div>
            </div> 

            <div class="row">              
               
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Expenses Cost</h4>
                    <p>{{ $hotel->expenses_cost}}</p>
                </div>
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Selling Cost</h4>
                    <p>{{ $hotel->selling_cost}}</p>
                </div>

                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Additional  Cost</h4>
                    <p>{{ $hotel->additional_cost}}</p>
                </div>

                 <div class="col-4 col-sm-12 col-md-4">
                    <h4>Total Cost</h4>
                    <p>{{ $hotel->totalcost}}</p>
                </div>
            </div> 
            <div class="row">
                
                <div class="col-4 col-sm-12 col-md-4">
                    <h4>Description</h4>
                    <p>{!! $hotel->description !!}</p>
                </div>
            </div>

 <div class="row">
                <a href="{{ route('hotel.index') }}">
                    <button class="btn btn-info">Go Back</button>
                </a>
            </div>
            
           
           
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </div>
     <!-- /.box-body -->
   </div>
   <!-- /.box -->

 </section>
 <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
			$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
		}(window, document, jQuery);
    </script>

    <script type="text/javascript">
        @if(session('flash_message'))
 swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
@endsection
