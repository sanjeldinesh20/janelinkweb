@extends('admin.layouts.admin_design')

@section('title')
<title>Edit hotel - Jane Link Travel</title> 
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     Edit hotel
   </h1>
   <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
     <li class="breadcrumb-item"><a href="{{route('hotel.index')}}">View All</a></li>
     <li class="breadcrumb-item active">Edit hotel</li>
   </ol>
 </section>

 <section class="content">

  <!-- Basic Forms -->
   <div class="box box-solid box-info">
     <div class="box-header with-border">
       <h6 class="box-subtitle text-white"> hotel Details </h6>
     </div>
     <!-- /.box-header -->
     <div class="box-body">
       <div class="row">
         <div class="col">
           

            <form action="{{route('hotel.update',$hotel->id)}}" method="post" enctype="multipart/form-data">
             @csrf
              <div class="form-group">
         <h5>Date:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
             <?php $dv = Carbon\Carbon::today(); ?>
           <input type="date" name="book_date" id="book_date" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $dv->toDateString() }}"> </div>
       </div>
       <div class="form-group">
      <h5>Type of hotel: &nbsp;<span class="text-danger">*</span></h5>
      <select name="service_id" id="service" class="form-control">                
          <option disabled selected value>Select One Service</option>
          @foreach($service as $service_data)   
          <option value="{{ $service_data->id }}">{{ $service_data->service_name }}</option>
          @endforeach         
      </select>
     </div>
       <div class="form-group">
         <h5>Full Name:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="full_name" id="name" class="form-control" data-validation-required-message=" Title Field is required" value="{{$hotel->full_name }}"> </div>
       </div>

        <div class="form-group">
         <h5>Age:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="age" id="age" class="form-control age" data-validation-required-message=" Title Field is required" value="{{ $hotel->age }}"> </div>
       </div>
          <div class="form-group">
            <h5>Select Gender: &nbsp;<span class="text-danger">*</span></h5>
            <select name="gender" id="gender" class="form-control">                
                <option disabled selected value>Select Gender</option>
                <option value="male">Male</option>                
                <option value="female">Female</option>
                <option value="other">Other</option>
            </select>
        </div>
        <div class="form-group">
            <h5>Select Country &nbsp;<span class="text-danger">*</span></h5>
            <select name="country_id" id="country_id" class="form-control">
            <option disabled selected value>Select Coountry</option>
                @foreach($country as $country_data)                
                <option value="{{ $country_data->id }}">{{ $country_data->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
         <h5> Address:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="address" id="address" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $hotel->address }}"> </div>
       </div>

        <div class="form-group">
         <h5> Email:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="email" name="email" id="email" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $hotel->email}}"> </div>
       </div>

        <div class="form-group">
         <h5> Phone No.&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="phone_number" id="phone" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $hotel->phone_number }}"> </div>
       </div>
        <div class="form-group">
         <h5>Arrival - Date:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <?php $dv = Carbon\Carbon::today(); ?>
           <input type="date" name="arrival_date" id="date" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $dv->toDateString() }}"> </div>
       </div>
        <div class="form-group">
         <h5>Departure - Date:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <?php $dv = Carbon\Carbon::today(); ?>
           <input type="date" name="departure_date" id="date" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $dv->toDateString() }}"> </div>
       </div>
      <div class="form-group">
         <h5> Number of Adults:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="number_adults" id="adult" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $hotel->number_adults }}"> </div>
       </div>
       <div class="form-group">
         <h5> Number of Kids (If there are any):&nbsp;<span class="text-danger"></span></h5>
         <div class="controls">
           <input type="number" name="number_kids" id="kids" class="form-control"  value="{{ $hotel->number_kids }}"> </div>
       </div>
       <div class="form-group">
         <h5> Number of Nights at Hotel:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="nights_at_hotel" id="night" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $hotel->nights_at_hotel}}"> </div>
       </div>
        <div class="form-group">
         <h5> Expenses Cost :&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="expenses_cost" id="price" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $hotel->expenses_cost}}"> </div>
       </div>

       <div class="form-group">
         <h5> Selling Cost :&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="selling_cost" id="price" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $hotel->selling_cost}}"> </div>
       </div>

        <div class="form-group">
        <h5> Additional Cost :&nbsp;<span class="text-danger">*</span></h5>
        <div class="controls">
          <input type="number" name="additinal_cost" id="price" class="form-control" data-validation-required-message=" Title Field is required" value="{{ $hotel->total_cost}}"> </div>
      </div>
       


        <div class="form-group">
          <h5>Description:</h5>
          <div class="controls">
              <textarea id="editor1" name="description" rows="10" cols="80">
{!! $hotel->description !!}
              </textarea>
        </div>
       </div>

          
        <br>
        <div class="text-xs-right text-center">
            <button type="submit" class="btn btn-info btn-lg">Submit</button>
          </div>  
     </form>

         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </div>
     <!-- /.box-body -->
   </div>
   <!-- /.box -->

 </section>
 <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
      $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
    </script>

    <script type="text/javascript">
        @if(session('flash_message'))
 swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
    <script type="text/javascript">
      $('.yyy').click(function(){
        console.log('hello')
        if('button[class=active]'){
          $('#abc').val('T')
        }
        else{
           $('#abc').val('F')
        }
      });
    </script>
    <script type="text/javascript">
      
      

$(function() {
    $("#salary1, #salary2, #salary3" ).on("keydown keyup", sum);
  function sum() {
  $("#salarysum").val(Number($("#salary1").val()) + Number($("#salary2").val()) + Number($("#salary3").val()));
  }
});
    </script>

     <script type="text/javascript">      
        $('.age').change(function(){
         var age = document.getElementById('age').value
         var bDay = moment().subtract(age, 'years');
         var bvalue =moment(bDay).format('MM/DD/YYYY');
         console.log(bvalue);
         document.getElementById('dob').value = bvalue;      
        });
      </script>
@endsection
