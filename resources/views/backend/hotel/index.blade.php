@extends('admin.layouts.admin_design')

@section('title')
<title>View hotel - Jane Link Travel</title> 
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          View hotel
        </h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
          <li class="breadcrumb-item"><a href="{{route('hotel.create')}}">Add New</a></li>
          <li class="breadcrumb-item active">View</li>
        </ol>
      </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
         
         <div class="box box-solid box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">hotel Details</h4>
            </div>
            <!-- /.box-header -->
        <div class="box-body">
				<div class="table-responsive">
				  <table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>S.N</th>
							<th>Book Date</th>
							<th> Full Name</th>
              <th> Phone No</th>
              <th> Arrival - Date</th>
              <th> Number of Adults</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
            @foreach($hotel as $hotel_data)           
						<tr>
							<td>{{ $loop->index+1 }}</td>
              <td>{{ $hotel_data->book_date }}</td>
              <td>{{ $hotel_data->full_name }}</td>
              <td> {{ $hotel_data->phone_number }}</td>

              <td> {{ $hotel_data->arrival_date }}</td>
              <td> {{ $hotel_data->number_adults }}</td>
              

             
							<td>
                <a href="{{ route('hotel.edit',$hotel_data->id) }}" title="Edit" data-toggle="tooltip">
                    <i class="fas fa-edit fa-2x text-info"></i> 
                </a>
                &nbsp;&nbsp;
                  <a href="{{ route('hotel.show',$hotel_data->id) }}" title="Show" data-toggle="tooltip">
                    <i class="far fa-eye fa-2x text-success"></i> 
                </a>
                 
                &nbsp;&nbsp;
                  <a href="{{URL::to('/admin/printhotel/'.$hotel_data->id)}}" class="btnprint" title="print" data-toggle="tooltip">
                    <i class="fas fa-print fa-2x text-dark"></i>
                </a>
                &nbsp;&nbsp;
                <a  href="javascript:" rel="{{$hotel_data->id}}" rel1="hotel/destroy" class="text-inverse deleteRecord" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                    <i class="fas fa-trash-alt fa-2x text-danger"></i>
                </a>
              </td>
            </tr>
            @endforeach
           
					</tbody>
					
				  </table>
				</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
                  
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection



@section('scripts')

<script type="text/javascript"> 
$(document).ready(function(){
  $('.btnprint').printPage();
});
</script>
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
			$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
		}(window, document, jQuery);
    </script>


    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>

@endsection