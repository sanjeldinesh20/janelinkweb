@extends('admin.layouts.admin_design')

@section('title')
<title> Hotel Booking Details - Janelink - </title> 
@endsection

@section('content')


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Invoice
        <small>Bill NO: 0154879</small>
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="#">{{ $hotel->full_name }}</a></li>
        <li class="breadcrumb-item active">Invoice</li>
      </ol>
    </section>

    

    <!-- Main content -->
    <div id="printpage">
    <section class="invoice printableArea">
      
        <!-- title row -->
        <div class="row">
       
        <div class="col-12">
          <div class="page-header">
          <h2 class="d-inline"><span class="font-size-30">JaneLink Travel Pvt. Ltd.</span></h2>
          <div class="pull-right text-right">
            <h3>{{ $hotel ->book_date }}</h3>
          </div>  
          </div>
        </div>
        <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
        <div class="col-md-6 invoice-col">
          <strong>From</strong> 
          <address>
          <strong class="text-blue font-size-24">JaneLink Travel</strong><br>
          <strong class="d-inline">Japan -Tokiyo</strong><br>
          <strong>Phone: 9874563210 &nbsp;&nbsp;&nbsp;&nbsp; Email: info@janelinktravel.com</strong>  
          </address>
        </div>
        <!-- /.col -->
        <div class="col-md-6 invoice-col text-right">
          <strong>To</strong>
          <address>
          <strong class="text-blue font-size-24">{{ $hotel ->full_name}}</strong><br>
         {{ $hotel->country->name }},{{ $hotel ->address}}<br>
          <strong>Age: {{ $hotel->phone_number }} &nbsp;&nbsp;&nbsp;&nbsp; Email: {{ $hotel->email }}</strong>
          <strong>Phone: {{ $hotel->phone_number }} &nbsp;&nbsp;&nbsp;&nbsp; gender: {{ $hotel->gender }}</strong>

          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-12 invoice-col mb-15">
          <div class="invoice-details row no-margin">
            <div class="col-md-6 col-lg-3"><b>Service :</b>{{ $hotel->service->service_name}}</div>
            <div class="col-md-6 col-lg-3"><b>Arrival - Date:</b> {{ $hotel ->arrival_date}}</div>
            <div class="col-md-6 col-lg-3"><b>Departure - Date:</b> {{ $hotel->departure_date }}</div>
            

          </div>
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
        <div class="col-12 table-responsive">
          <table class="table table-bordered">
          <tbody>
          <tr>
            <th>Full Name</th>
            <th>Number of Adults</th>
            <th>Number of Kids</th>            
            <th>Description</th>
            <th class="text-right">Total Cost</th>
            
          </tr>
          <tr>
            <td>{{ $hotel->full_name}}</td>
            <td>{{ $hotel->number_adults}}</td>
            <td>{{ $hotel->number_kids}}</td>
            <td>{!! $hotel->description !!}</td>            
            <td class="text-right">{{ $hotel->totalcost}}</td>
            
          </tr>
          
          
          </tbody>
          </table>
        </div>
        <!-- /.col -->
        </div>
        
          <div class="row no-print plink" >
        <div class="col-xs-12">
          <a href="" class="btn btn-primary" onclick="printdiv()"><i class="fa fa-print"></i> Print</a>
           <div class = "row col-md-3 pull-right">
        <a href="{{ route('hotel.index') }}" class="btn btn-primary pull-right">Cancel</a>
     </div>
        </div>
      </div>
    </section>

</div>
</div>
 

 


<!-- /.content-wrapper -->
@endsection


@section('scripts')

<script type="text/javascript">
    function printdiv()
    {
    //your print div data
    //alert(document.getElementById("printpage").innerHTML);
    $('.plink').hide();
    var newstr=document.getElementById("printpage").innerHTML;

    var header='<link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"><link href="{{ asset('fonts/font-awesome.min.css') }}" rel="stylesheet"><link href="{{ asset('print/print.css') }}" rel="stylesheet">'

    var footer =''
    var popupWin = window.open('', '_blank', 'width=1100,height=600');
    popupWin.document.open();
    popupWin.document.write('<html>'+header+'<body onload="window.print()">'+ newstr + '</html>');
    popupWin.document.close(); 
    return false;
    }
</script>
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
			$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
		}(window, document, jQuery);
    </script>

    <script type="text/javascript">
        @if(session('flash_message'))
 swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
@endsection
