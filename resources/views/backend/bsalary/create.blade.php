@extends('admin.layouts.admin_design')

@section('title')
<title>Add Basic Salary - Jane Link Travel</title>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     Add New Basic Salary
   </h1>
   <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
     <li class="breadcrumb-item"><a href="{{route('basic.index')}}">View All</a></li>
     <li class="breadcrumb-item active">Add Advance</li>
   </ol>
 </section>

 <section class="content">

  <!-- Basic Forms -->
   <div class="box box-solid box-info">
     <div class="box-header with-border">
       <h6 class="box-subtitle text-white"> Basic Salary Details </h6>
     </div>
     <!-- /.box-header -->
     <div class="box-body">
       <div class="row">
         <div class="col">
           <form action="{{route('basic.store')}}" method="post" enctype="multipart/form-data">
             @csrf
             <div class="form-group">
            <h5>Select Employee  Name: &nbsp;<span class="text-danger">*</span></h5>
            <select name="employee_id" id="employee_id" class="form-control">
               @foreach($employee as $emplo) 
                <option value="{{ $emplo->id}}">{{ $emplo->employee_name}}</option>
                 @endforeach
            </select>
                 <a href="" style="color: red;">{{ $errors -> first('employee_id') }}</a>
             </div>

      <div class="form-group">
         <h5>Basic Salary:&nbsp;<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="number" name="basic_amount"  id="basic" class="form-control" data-validation-required-message=" Title Field is required" value="{{old('basic_amount')}}"> </div>
       </div>

       
        <div class="form-group">
          <h5>Description:</h5>
          <div class="controls">
              <textarea id="editor1" name="description" rows="10" cols="80">
              </textarea>                           
        </div>
       </div>

          
        <br>
        <div class="text-xs-right text-center">
            <button type="submit" class="btn btn-info btn-lg">Submit</button>
          </div>  
     </form>

         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </div>
     <!-- /.box-body -->
   </div>
   <!-- /.box -->

 </section>
 <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
			$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
		}(window, document, jQuery);
    </script>

    <script type="text/javascript">
        @if(session('flash_message'))
 swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
    <script type="text/javascript">
      $('.yyy').click(function(){
        console.log('hello')
        if('button[class=active]'){
          $('#abc').val('T')
        }
        else{
           $('#abc').val('F')
        }
      });
    </script>

     
@endsection
