<!-- jQuery 3 -->
<script src="{{asset('public/adminpanel/assets/vendor_components/jquery/dist/jquery.js')}}"></script>

<!-- popper -->
{{--<script src="{{asset('public/adminpanel/assets/vendor_components/popper/dist/popper.min.js')}}"></script>--}}

<!-- Bootstrap 4.0-->
<script src="{{asset('public/adminpanel/assets/vendor_components/bootstrap/dist/js/bootstrap.js')}}"></script>

<!-- Slimscroll -->
<script src="{{asset('public/adminpanel/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

<!-- FastClick -->
<script src="{{asset('public/adminpanel/assets/vendor_components/fastclick/lib/fastclick.js')}}"></script>

<!-- apexcharts -->
<script src="{{asset('public/adminpanel/assets/vendor_components/apexcharts-bundle/irregular-data-series.js')}}"></script>
<script src="{{asset('public/adminpanel/assets/vendor_components/apexcharts-bundle/dist/apexcharts.js')}}"></script>

<!-- NeoX Admin App -->
<script src="{{asset('public/adminpanel/js/template.js')}}"></script>

<!-- NeoX Admin dashboard demo (This is only for demo purposes) -->
<script src="{{asset('public/adminpanel/js/pages/dashboard3.js')}}"></script>

<!-- NeoX Admin for demo purposes -->
<script src="{{asset('public/adminpanel/js/demo.js')}}"></script>

<script src="{{asset('public/adminpanel/assets/vendor_components/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('public/adminpanel/assets/vendor_components/sweetalert/jquery.sweet-alert.custom.js')}}"></script>


<!-- CK Editor -->
<script src="{{ asset('public/adminpanel/assets/vendor_components/ckeditor/ckeditor.js') }}"></script>

<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('public/adminpanel/assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js') }}"></script>

<!-- NeoX Admin for editor -->
<script src="{{ asset('public/adminpanel/js/pages/editor.js') }}"></script>
	
<!-- NeoX Admin for Data Table -->
<script src="{{ asset('public/adminpanel/js/pages/data-table.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('public/adminpanel/assets/vendor_components/datatable/datatables.min.js') }}"></script>
<!-- moment.min -->
<script src="{{ asset('public/adminpanel/assets/vendor_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/adminpanel/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

{{--<script src="{{ asset('public/adminpanel/assets/js/pages/advanced-form-element.js') }}"></script>--}}


<!-- bootstrap datepicker -->
    <script src="{{ asset('public/adminpanel/assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    

{{--SweetAlert--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script>
    $(document).ready(function () {
        $(".deleteRecord").click(function(){
            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-danger',
                    cancelButtonClass: 'btn btn-danger',
                    buttonStyling: false,
                    reverseButtons: true
                },
                function () {
                    window.location.href="/janelink/admin/"+deleteFunction+"/"+id;

                });
        });
    });
</script>
{{-- sweetalert --}}

@yield('scripts')
