<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="icon" href="">

@yield('title')
<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="{{asset('public/adminpanel/assets/vendor_components/bootstrap/dist/css/bootstrap.css')}}">

<!-- Bootstrap extend-->
<link rel="stylesheet" href="{{asset('public/adminpanel/css/bootstrap-extend.css')}}">

<!-- theme style -->
<link rel="stylesheet" href="{{asset('public/adminpanel/css/master_style.css')}}">

<!-- NeoX Admin skins -->
<link rel="stylesheet" href="{{asset('public/adminpanel/css/skins/_all-skins.css')}}">


<link href="{{asset('public/adminpanel/assets/vendor_components/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">

<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('public/adminpanel/assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css') }}">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="{{ asset('public/adminpanel/assets/vendor_components/bootstrap-switch/switch.css') }}">

<!-- daterange picker -->	
	<link rel="stylesheet" href="{{ asset('public/adminpanel/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css')}}">

	<!-- bootstrap datepicker -->	
	<link rel="stylesheet" href="{{ asset('public/adminpanel/assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

