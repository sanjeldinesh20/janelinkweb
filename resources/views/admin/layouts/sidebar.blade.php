<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">

        <!-- sidebar menu-->
        <ul class="sidebar-menu" data-widget="tree">
            
            {{-- Dashboard --}}
            <li class="">
                <a href="{{route('dashboard')}}">
                    <i class="fas fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            {{-- Dashboard --}}
         {{-- Site Settings --}}
            <li class="">
                <a href="{{route('sitesetting.edit',1)}}">
                    <i class="far fa-sun"></i>
                    <span>Site Settings</span>
                </a>
            </li>   
            {{-- Site Settings --}}


            {{-- Service --}}
            <li class="treeview">
                <a href="#">
                    <i class="fas fa-newspaper"></i>
                    <span>Service</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('service.index')}}"><i class="iconsmind-Arrow-Through"></i> Service</a></li>
                </ul>
            </li>   
            {{-- Service --}}
       
                {{-- Partner --}}
            <li class="treeview">
                <a href="#">
                    <i class="fas fa-newspaper"></i>
                    <span>Partner</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('partner.index')}}"><i class="iconsmind-Arrow-Through"></i> Partner</a></li>
                </ul>
            </li>   
            {{-- Partner --}}



            {{-- Visitor --}}
            <li class="treeview">
                <a href="#">
                    <i class="fas fa-newspaper"></i>
                    <span>Visitor</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('visitor.index')}}"><i class="iconsmind-Arrow-Through"></i> Visitor</a></li>
                </ul>
            </li>   
            {{-- Visitor --}}


             {{-- Employee --}}
            <li class="treeview">
                <a href="#">
                    <i class="fas fa-newspaper"></i>
                    <span>Employee</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('employee.index')}}"><i class="iconsmind-Arrow-Through"></i> Add  Employee</a></li>
                    <li><a href="{{route('basic.create')}}"><i class="iconsmind-Arrow-Through"></i> Add   Basic Salary</a></li>
                    <li><a href="{{route('salary.index')}}"><i class="iconsmind-Arrow-Through"></i>Employee Salary</a></li>
                    <li><a href="{{route('advance.index')}}"><i class="iconsmind-Arrow-Through"></i>Advance payment</a></li>

                </ul>
            </li>   
            {{-- Employee --}}

             {{-- Expense --}}
            <li class="treeview">
                <a href="#">
                    <i class="fas fa-newspaper"></i>
                    <span>Expense</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                        <li><a href="{{route('expense.create')}}"><i class="iconsmind-Arrow-Through"></i>Add Dailly Expense</a></li>

                </ul>
            </li>   
            {{-- Expense --}}



             {{-- Booking --}}
            <li class="treeview">
                <a href="#">
                    <i class="fas fa-newspaper"></i>
                    <span>Ticket Booking</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                   {{-- <li><a href="{{route('hotel.index')}}"><i class="iconsmind-Arrow-Through"></i>Hotel Booking</a></li>--}}
                    <li><a href="{{route('ticket.create')}}"><i class="iconsmind-Arrow-Through"></i>Ticket Booking Add</a></li>

                    <li><a href="{{route('ticket.index')}}"><i class="iconsmind-Arrow-Through"></i>Ticket Booking List</a></li>
                    <li><a href="{{route('ticket.report')}}"><i class="iconsmind-Arrow-Through"></i>Ticket Booking Report</a></li>

                </ul>
            </li>   
            {{-- Booking --}}


            {{-- Report --}}
            <li class="treeview">
                <a href="#">
                    <i class="fas fa-newspaper"></i>
                    <span>Report</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('day.report')}}"><i class="iconsmind-Arrow-Through"></i>Daily Expense Report</a></li>
                    <li><a href="{{route('report.index')}}"><i class="iconsmind-Arrow-Through"></i> View Dailly Expense</a></li>
                    {{--<li><a href="{{route('report.booking')}}"><i class="iconsmind-Arrow-Through"></i>Hotel Booking Report</a></li>--}}
                    <li><a href="{{route('report.ticket')}}"><i class="iconsmind-Arrow-Through"></i>Ticket Booking Report</a></li>

                </ul>
            </li>
            {{-- Report --}}





        </ul>

    </section>
</aside>

