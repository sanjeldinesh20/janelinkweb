<header class="main-header">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Logo -->
    <a href="{{route('dashboard')}}" class="logo">
        <!-- mini logo -->
        <b class="logo-mini">
            <span class="light-logo"><img src="{{asset('public/adminpanel/images/logo-dark-text.png')}}" alt="logo" width="220"></span>
            {{-- <span class="dark-logo"><img src="{{asset('public/adminpanel/images/logo-dark-text.png')}}" alt="logo"></span> --}}
        </b>
        <!-- logo-->
        <span class="logo-lg">
			  {{-- <img src="{{asset('public/adminpanel/images/logo-light-text.png')}}" alt="logo" class="light-logo"> --}}
			  {{-- <img src="{{asset('public/adminpanel/images/logo-dark-text.png')}}" alt="logo" class="dark-logo"> --}}
		  </span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <li class="search-box">
                    <a class="nav-link hidden-sm-down" href="javascript:void(0)"><i class="iconsmind-Magnifi-Glass2"></i></a>
                    <form class="app-search" style="display: none;">
                        <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="ti-close"></i></a>
                    </form>
                </li>
                <!-- Notifications -->
                {{-- <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="iconsmind-Bell"></i>
                    </a>
                    <ul class="dropdown-menu scale-up">
                        <li class="header">You have 7 notifications</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu inner-content-div">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i> Curabitur id eros quis nunc suscipit blandit.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-warning text-yellow"></i> Duis malesuada justo eu sapien elementum, in semper diam posuere.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-red"></i> Donec at nisi sit amet tortor commodo porttitor pretium a erat.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-shopping-cart text-green"></i> In gravida mauris et nisi
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user text-red"></i> Praesent eu lacus in libero dictum fermentum.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user text-red"></i> Nunc fringilla lorem
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user text-red"></i> Nullam euismod dolor ut quam interdum, at scelerisque ipsum imperdiet.
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li> --}}
                <!-- User Account-->
                <li class="dropdown user user-menu" style="margin-right:30px">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="iconsmind-User"></i>
                    </a>
                    <?php
                      $current_user = Auth::user();
                     ?>
                    <ul class="dropdown-menu scale-up">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{asset('public/uploads/user/small/'.$current_user->userprofile->avatar)}}" class="float-left rounded-circle" alt="User Image">
                            <p>
                                {{$current_user->name}}
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="row no-gutters">
                                <div>{{$current_user->email}}</div>
                                <div class="col-12 text-left">
                                    <a href="{{route('profile', $current_user->id)}}"><i class="ion ion-person"></i> My Profile</a>
                                </div>
                                {{-- <div class="col-12 text-left">
                                    <a href="#"><i class="ion ion-email-unread"></i> Inbox</a>
                                </div>
                                <div class="col-12 text-left">
                                    <a href="#"><i class="ion ion-settings"></i> Setting</a>
                                </div> --}}
                                <div role="separator" class="divider col-12"></div>
                                <div class="col-12 text-left">
                                    <a href="{{route('edit.password')}}"><i class="ti-settings"></i> Account Setting</a>
                                </div>
                                <div role="separator" class="divider col-12"></div>
                                <div class="col-12 text-left">
                                    <a href="{{route('admin.logout')}}"><i class="fa fa-power-off"></i> Logout</a>
                                </div>
                            </div>
                            <!-- /.row -->
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->

            </ul>
        </div>
    </nav>
</header>
