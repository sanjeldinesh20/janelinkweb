@extends('admin.layouts.admin_design')

@section('title')
<title>Dashboard - Jane Link Travel</title>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">


        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="row no-gutters py-2">

                            <div class="col-sm-6 col-lg-3">
                                <div class="box-body br-1 border-light">
                                    <div class="flexbox mb-1">
						  <span>
							<i class="ion-person font-size-26"></i><br>
							Total Visitor
						  </span>
                                        <span class="text-primary font-size-40">{{$total_visitor}}</span>




                                    </div>
                                    <div class="progress progress-xxs mt-10 mb-0">
                                        <div class="progress-bar" role="progressbar" style="width: 35%; height: 4px;" aria-valuenow="{{$total_visitor}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-6 col-lg-3 hidden-down">
                                <div class="box-body br-1 border-light">
                                    <div class="flexbox mb-1">
						  <span>
							<i class="ion-document font-size-26"></i><br>
							Today Booking
						  </span>
                                        <span class="text-info font-size-40">{{$count_ticketing}}</span>
                                    </div>
                                    <div class="progress progress-xxs mt-10 mb-0">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 55%; height: 4px;" aria-valuenow="{{$count_ticketing}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-6 col-lg-3 d-none d-lg-block">
                                <div class="box-body br-1 border-light">
                                    <div class="flexbox mb-1">
						  <span>
							<i class="ion-information font-size-26"></i><br>
							Number of Partner
						  </span>
                                        <span class="text-warning font-size-40">{{$total_partner}}</span>
                                    </div>
                                    <div class="progress progress-xxs mt-10 mb-0">
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: 65%; height: 4px;" aria-valuenow="{{$total_partner}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-6 col-lg-3 d-none d-lg-block">
                                <div class="box-body">
                                    <div class="flexbox mb-1">
						  <span>
							<i class="ion-folder font-size-26"></i><br>
							Number of Employee
						  </span>
                                        <span class="text-danger font-size-40">{{$total_employee}}</span>
                                    </div>
                                    <div class="progress progress-xxs mt-10 mb-0">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: 40%; height: 4px;" aria-valuenow="{{$total_employee}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>



                        </div>


                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="row no-gutters py-2">
                            @foreach($expenses_total as $key => $expensestotal)
                                @if($key == 0)
                            <div class="col-sm-6 col-lg-3">
                                <div class="box-body br-1 border-light">
                                    <div class="flexbox mb-1">
						  <span>
							<i class="ion-person font-size-26"></i><br>
							Today Total office Expenses
						  </span>
                              <span class="text-primary font-size-40">{{$expensestotal->total_expense}}</span>

                                    </div>
                                    <div class="progress progress-xxs mt-10 mb-0">
                                        <div class="progress-bar" role="progressbar" style="width: 35%; height: 4px;" aria-valuenow="{{$expensestotal->total_expense}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                                @endif
                            @endforeach
                                @foreach($totalticket as $key => $totaltic)
                                    @if($key == 0)
                            <div class="col-sm-6 col-lg-3 hidden-down">
                                <div class="box-body br-1 border-light">
                                    <div class="flexbox mb-1">
						  <span>
							<i class="ion-document font-size-26"></i><br>
							Today Total Ticketing Booking Expenses
						  </span>
                                        <span class="text-info font-size-40">{{$totaltic->total_ticketings}}</span>
                                    </div>
                                    <div class="progress progress-xxs mt-10 mb-0">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 55%; height: 4px;" aria-valuenow="{{$totaltic->total_ticketings}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                                    @endif
                                @endforeach
                                @foreach($totalticketsell as $key => $sell)
                                    @if($key == 0)

                            <div class="col-sm-6 col-lg-3 d-none d-lg-block">
                                <div class="box-body br-1 border-light">
                                    <div class="flexbox mb-1">
						  <span>
							<i class="ion-information font-size-26"></i><br>
							Today Total Ticketing Selling Cost

						  </span>
                                        <span class="text-warning font-size-40">{{$sell->total_sell}}</span>
                                    </div>
                                    <div class="progress progress-xxs mt-10 mb-0">
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: 65%; height: 4px;" aria-valuenow="{{$sell->total_sell}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                                    @endif
                                @endforeach

                            <div class="col-sm-6 col-lg-3 d-none d-lg-block">
                                <div class="box-body">

                                    <div class="flexbox mb-1">
						  <span>
							<i class="ion-folder font-size-26"></i><br>
							Today Total profit
						  </span>
                                        <span class="text-danger font-size-40">22</span>
                                    </div>
                                    <div class="progress progress-xxs mt-10 mb-0">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: 40%; height: 4px;" aria-valuenow="22" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


            <div id='calendar'></div>
        </section>




        <!-- /.content -->
    </div>
    @endsection

@section('scripts')
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />



    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
    <script>
        $(document).ready(function() {
            // page is now ready, initialize the calendar...
            $('#calendar').fullCalendar({
                // put your options and callbacks here
                events : [
                        @foreach($ticketing as $ticket)
                    {
                        title : '{{ $ticket->full_name }}',
                        start : '{{ $ticket->flight_date }}',
                        url : ''
                    },
                    @endforeach
                ]
            })
        });
    </script>

@endsection
