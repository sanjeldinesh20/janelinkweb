@extends('admin.layouts.admin_design')

@section('title')
<title>Profile - Jane Link Travel</title> 
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     My Profile
   </h1>
   <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="iconsmind-Library"></i></a></li>
     <li class="breadcrumb-item"><a href="javascript:">Admin</a></li>
     <li class="breadcrumb-item active">My Profile</li>
   </ol>
 </section>

 <!-- Main content -->
 <section class="content">

  <!-- Basic Forms -->
   <div class="box box-solid box-info">
     <div class="box-header with-border">
       <h6 class="box-subtitle text-white">{{ $user->name }}'s Profile Details</h6>
     </div>
     <!-- /.box-header -->
     <div class="box-body">
       <div class="row">
         <div class="col">
           <form action="{{route('update.profile', $user->id)}}" method="post" enctype="multipart/form-data">
             @csrf
       <div class="form-group">
         <h5>Name<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="text" name="name" id="name" class="form-control" data-validation-required-message="This field is required" value="{{$user->name}}"> </div>
       </div>

       <div class="form-group">
         <h5>Email<span class="text-danger">*</span></h5>
         <div class="controls">
           <input type="email" name="email" id="email" class="form-control"  data-validation-required-message="This field is required" value="{{$user->email}}"> </div>
       </div>

       <div class="form-group">
         <h5>Image</h5>
         <div class="controls">
           <input type="file" name="avatar" class="form-control"> </div>
           <br>
           <input type="hidden" name="current_avatar" value="{{$user->userprofile->avatar}}">
           <img src="{{asset('public/uploads/user/small/'.$user->userprofile->avatar)}}" alt="{{$user->name}}" width="100px">
       </div>

       <div class="form-group">
          <h5>Phone Number<span class="text-danger">*</span></h5>
          <div class="controls">
            <input type="text" name="phone" id="phone" class="form-control"  data-validation-required-message="This field is required" value="{{$user->userprofile->phone}}"> </div>
        </div>
        
       <div class="form-group">
          <h5>Facebook</h5>
          <div class="controls">
            <input type="text" name="facebook" id="facebook" class="form-control"   value="{{$user->userprofile->facebook}}"> </div>
        </div>
        
       <div class="form-group">
          <h5>Twitter</h5>
          <div class="controls">
            <input type="text" name="twitter" id="twitter" class="form-control"   value="{{$user->userprofile->twitter}}"> </div>
        </div>
        
       <div class="form-group">
          <h5>Instagram</h5>
          <div class="controls">
            <input type="text" name="instagram" id="instagram" class="form-control"   value="{{$user->userprofile->instagram}}"> </div>
        </div>
        
       <div class="form-group">
          <h5>Linkedin</h5>
          <div class="controls">
            <input type="text" name="linkedin" id="linkedin" class="form-control"   value="{{$user->userprofile->linkedin}}"> </div>
        </div>
        
       <div class="form-group">
          <h5>About Yourself</h5>
          <div class="controls">
              <textarea id="editor1" name="about" rows="10" cols="80">
                  {!! $user->userprofile->about !!}
              </textarea>
        </div>
       </div>

        <br>
        <div class="text-xs-right text-center">
            <button type="submit" class="btn btn-info btn-lg">Submit</button>
          </div>  
     </form>

         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->
     </div>
     <!-- /.box-body -->
   </div>
   <!-- /.box -->

 </section>
 <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection


@section('scripts')
<!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
			$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
		}(window, document, jQuery);
    </script>


    <script type="text/javascript">
        @if(session('flash_message'))
 swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
@endsection
