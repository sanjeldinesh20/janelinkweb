<?php return array (
  'sans-serif' => array(
    'normal' => $rootDir . '\lib\fonts\Helvetica',
    'bold' => $rootDir . '\lib\fonts\Helvetica-Bold',
    'italic' => $rootDir . '\lib\fonts\Helvetica-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $rootDir . '\lib\fonts\Helvetica',
    'bold' => $rootDir . '\lib\fonts\Helvetica-Bold',
    'italic' => $rootDir . '\lib\fonts\Helvetica-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $rootDir . '\lib\fonts\ZapfDingbats',
    'bold' => $rootDir . '\lib\fonts\ZapfDingbats',
    'italic' => $rootDir . '\lib\fonts\ZapfDingbats',
    'bold_italic' => $rootDir . '\lib\fonts\ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $rootDir . '\lib\fonts\Symbol',
    'bold' => $rootDir . '\lib\fonts\Symbol',
    'italic' => $rootDir . '\lib\fonts\Symbol',
    'bold_italic' => $rootDir . '\lib\fonts\Symbol',
  ),
  'serif' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSans-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSans-BoldOblique',
    'italic' => $rootDir . '\lib\fonts\DejaVuSans-Oblique',
    'normal' => $rootDir . '\lib\fonts\DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSansMono-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSansMono-BoldOblique',
    'italic' => $rootDir . '\lib\fonts\DejaVuSansMono-Oblique',
    'normal' => $rootDir . '\lib\fonts\DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSerif-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSerif-BoldItalic',
    'italic' => $rootDir . '\lib\fonts\DejaVuSerif-Italic',
    'normal' => $rootDir . '\lib\fonts\DejaVuSerif',
  ),
  'fontawesome' => array(
    'normal' => $fontDir . '\5979fca2635dacdc061f9e7b1c81339a',
  ),
  'ionicons' => array(
    'normal' => $fontDir . '\8fb5f658264471775bb1ae132bf26473',
  ),
  'themify' => array(
    'normal' => $fontDir . '\a1d89bbd7c72b44956acc49a267a2b04',
  ),
  'linea-arrows-10' => array(
    'normal' => $fontDir . '\769366aaa6e8c35524562b007ad466d3',
  ),
  'linea-basic-10' => array(
    'normal' => $fontDir . '\d1ac7c340e3646a2f6e98fcadba79f15',
  ),
  'linea-basic-elaboration-10' => array(
    'normal' => $fontDir . '\b1d7a7828b55db25d5c65c0f136fc50d',
  ),
  'linea-ecommerce-10' => array(
    'normal' => $fontDir . '\eb50763274ea5f2cb8a0041b7dc4d033',
  ),
  'linea-music-10' => array(
    'normal' => $fontDir . '\a67a011cce0686d2796baadd590b9b76',
  ),
  'linea-software-10' => array(
    'normal' => $fontDir . '\166467b0636589010ebce5a6a1a8a31b',
  ),
  'linea-weather-10' => array(
    'normal' => $fontDir . '\399fc2617c567d481d08dd727683c9f3',
  ),
  'glyphicons halflings' => array(
    'normal' => $fontDir . '\39c579722e04e373685a8a6120fc2c5f',
  ),
  'material design icons' => array(
    'normal' => $fontDir . '\0c9003d293973a10ec21bea35c2280d0',
  ),
  'simple-line-icons' => array(
    'normal' => $fontDir . '\441fe897efa27d7913a2d739efa30800',
  ),
  'icomoon' => array(
    'normal' => $fontDir . '\e912431c5c39ed9395acc66966ec3a18',
  ),
  'font awesome 5 brands' => array(
    'normal' => $fontDir . '\6ba687a83211543c7380cb18eca5204c',
  ),
  'font awesome 5 free' => array(
    'normal' => $fontDir . '\7d8aed7b74eacaec4b492173d3b2e5ff',
  ),
) ?>