<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticketing extends Model
{
    protected $appends = ['total'];

    public function service()
    {
        return $this->belongsTo('App\Service');

    }

    public function employee()
    {
        return $this->belongsTo('App\Employee');

    }

    public function getTotalAttribute()
    {
        return $this->number_passenger * $this->selling_cost + $this->additional_cost;
    }


}
