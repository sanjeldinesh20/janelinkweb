<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
      public function hotel()
    {
    	return $this->hasOne('App\Hotel');
    }
    public function ticketing()
    {
    	return $this->hasOne('App\Ticketing');
    }
}
