<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advance extends Model
{
    public function employee()
    { 
    	return $this->belongsTo('App\Employee');

    }

public function month()
    { 
    	return $this->belongsTo('App\Month');

    }
}
