<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $appends = ['totalcost'];

   public function service()
    { 
    	return $this->belongsTo('App\Service');

    }

     public function country()
    { 
    	return $this->belongsTo('App\Country');

    }


    public function getTotalcostAttribute()
    {
        return $this->selling_cost + $this->additional_cost;
    }
}
