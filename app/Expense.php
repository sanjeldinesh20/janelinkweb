<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed expense_date
 */
class Expense extends Model
{
//    protected  $appends=['total_expense'];



    public function employee()
    {
        return $this->belongsTo('App\Employee');

    }


//    public function getTotalExpenseAttribute()
//    {
//        return $this->sum('expense_price');
//    }
}
