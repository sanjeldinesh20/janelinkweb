<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\SiteSetting;
use App\Ticketing;
use View;
use App\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::composer(['*'], function($view){
            $view->with('sites_data', SiteSetting::whereId(1)->first());
        });

        View::composer(['*'], function($view){
            $view->with('ticketing', Ticketing::all());
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
