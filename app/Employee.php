<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
     public function salary()
    {
    	return $this->hasOne('App\Salary');
    }

    public function advance()
    {
    	return $this->hasOne('App\Advance');
    }

    public function expense()
    {
    	return $this->hasOne('App\Expense');
    }

    public function ticketing()
    {
        return $this->hasOne('App\Ticketing');
    }

    public function bsalary()
    {
        return $this->hasOne('App\Bsalary');
    }
}
