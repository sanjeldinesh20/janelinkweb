<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function ticketing()
    {
    	return $this->hasOne('App\Ticketing');
    }

     public function hotel()
    {
    	return $this->hasOne('App\Hotel');
    }
}
