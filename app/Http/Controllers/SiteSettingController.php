<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SiteSetting;
use Session;
use Auth;
use File;
use Image;
use Illuminate\Support\Facades\Input;
class SiteSettingController extends Controller
{

 public function edit($id)
    {
        $sites = SiteSetting::where('id',$id)->first();
        return view ('backend.setting.edit',compact('sites'));
    }

    public function update(Request $request, $id)
    {
        
        //Logo
        $image_tmp_logo = Input::file('organization_logo');

        if (!empty($image_tmp_logo)){
            if ($image_tmp_logo->isValid()) {
                $extension = $image_tmp_logo->getClientOriginalExtension();
                $filename_logo = rand(111, 99999) . '.' . $extension;
                $extra_large_image_path = 'public/uploads/setting/'.$filename_logo;
                $small_image_path = 'public/uploads/setting/small/'.$filename_logo;

                //Resize Images
                Image::make($image_tmp_logo)->save($extra_large_image_path);
                Image::make($image_tmp_logo)->resize(120,60)->save($small_image_path);

            }
        }else{
            $filename_logo = $request->current_logo;
        }
        $sites = SiteSetting::findOrFail($id);

        $sites->organization_name = $request->organization_name;
        $sites->organization_logo = $filename_logo;
        $sites->organization_address = $request->organization_address;
        $sites->organization_email = $request->organization_email;
        $sites->contact_number = $request->contact_number;
        $sites->chief = $request->chief;
        $sites->director = $request->director;
        $sites->tax = $request->tax;
        $sites->organization_facebook = $request->organization_facebook;
        $sites->organization_twitter = $request->organization_twitter;
        $sites->organization_linkedin = $request->organization_linkedin;
        $sites->organization_instagram = $request->organization_instagram;
       


        $sites->save();
        
        return redirect()->back()->with('flash_message', 'Site Settings has been updated Successfully');

    }
}
