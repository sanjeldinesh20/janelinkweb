<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Country;
use App\Hotel;
class HotelController extends Controller
{
    public function index()
    {
        $hotel = Hotel::all();

        return view ('backend.hotel.index')->with('hotel',$hotel);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        $service = Service::all();
        $country = Country::all();
        return view ('backend.hotel.create')->with('service',$service)->with('country',$country);

    }


     

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $hotel = new Hotel();

        
        $hotel->book_date = $request->book_date;
        $hotel->service_id = $request->service_id;
        $hotel->full_name = $request->full_name;
        $hotel->age = $request->age;
        $hotel->gender = $request->gender;
        $hotel->country_id = $request->country_id;
        $hotel->address = $request->address;
        $hotel->email = $request->email;
        $hotel->phone_number = $request->phone_number;
        $hotel->arrival_date = $request->arrival_date;
        $hotel->departure_date = $request->departure_date;
        $hotel->number_adults = $request->number_adults;
        $hotel->number_kids = $request->number_kids;
        $hotel->nights_at_hotel = $request->nights_at_hotel;
        $hotel->expenses_cost = $request->expenses_cost;
        $hotel->selling_cost = $request->selling_cost;        
        $hotel->additional_cost = $request->additional_cost;
        $hotel->description = $request->description;
           
        $hotel->save();

        return redirect()->route('hotel.print',$hotel->id)->with('flash_message', 'New hotel has been Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hotel = Hotel::where('id',$id)->first();
        return view ('backend.hotel.show',compact('hotel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   $service = Service::all();
        $country = Country::all();
        
        $hotel = Hotel::where('id',$id)->first();
        return view ('backend.hotel.edit',compact('hotel','service','country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hotel = Hotel::findOrFail($id);

        $hotel->book_date = $request->book_date;
        $hotel->service_id = $request->service_id;
        $hotel->full_name = $request->full_name;
        $hotel->age = $request->age;
        $hotel->gender = $request->gender;
        $hotel->country_id = $request->country_id;
        $hotel->address = $request->address;
        $hotel->email = $request->email;
        $hotel->phone_number = $request->phone_number;
        $hotel->arrival_date = $request->arrival_date;
        $hotel->departure_date = $request->departure_date;
        $hotel->number_adults = $request->number_adults;
        $hotel->number_kids = $request->number_kids;
        $hotel->nights_at_hotel = $request->nights_at_hotel;
        $hotel->expenses_cost = $request->expenses_cost;
        $hotel->selling_cost = $request->selling_cost;        
        $hotel->additional_cost = $request->additional_cost;
        $hotel->description = $request->description;

       

        $hotel->save();

        return redirect()->route('hotel.index')->with('flash_message', 'hotel has been Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $hotel = hotel::find($id);
                
        $hotel->delete();

        return redirect()->back()->with('flash_message', 'hotel has been Deleted Successfully');
    }



     public function print($id)
    {
        $hotel = Hotel::where('id',$id)->first();
        return view ('backend.hotel.print',compact('hotel'));
    }
}
