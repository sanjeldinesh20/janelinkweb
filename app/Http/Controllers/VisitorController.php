<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visitor;
use App\Country;
use App\Service;

class VisitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $visitor = Visitor::latest()->get();
        return view ('backend.visitor.index',compact('visitor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
        $country = Country::all();
        $service = Service::all();
        
        return view ('backend.visitor.create')->with('country',$country)->with('service',$service);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $visitor = new Visitor();

        

        $visitor->visitor_name = $request->visitor_name;
        $visitor->country_id = $request->country_id;
        $visitor->visitor_email = $request->visitor_email;
        $visitor->visitor_address = $request->visitor_address;        
        $visitor->visitor_contact = $request->visitor_contact;
        $visitor->visitor_passport = $request->visitor_passport;
        $visitor->service_id = $request->service_id;
        $visitor->description = $request->description;

       
        $visitor->save();

        return redirect()->route('visitor.index')->with('flash_message', 'New visitor has been Added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $country = Country::all();
        $service = Service::all();
        $visitor = Visitor::where('id',$id)->first();
        return view ('backend.visitor.edit',compact('visitor','service','country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $visitor = Visitor::findOrFail($id);


       $visitor->visitor_name = $request->visitor_name;
        $visitor->country_id = $request->country_id;
        $visitor->visitor_email = $request->visitor_email;
        $visitor->visitor_address = $request->visitor_address;        
        $visitor->visitor_contact = $request->visitor_contact;
        $visitor->visitor_passport = $request->visitor_passport;
        $visitor->service_id = $request->service_id;
        $visitor->description = $request->description;

       

        $visitor->save();

        return redirect()->route('visitor.index')->with('flash_message', 'visitor has been Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $visitor = Visitor::find($id);

        
        

        $visitor->delete();

        return redirect()->back()->with('flash_message', 'visitor has been Deleted Successfully');
    }
}
