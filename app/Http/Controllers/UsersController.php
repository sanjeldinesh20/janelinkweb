<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\UserProfile;
use Illuminate\Support\Facades\Hash;


class UsersController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::all();
        $user = User::latest()->get();
        return view ('backend.user.index',compact('user','role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Role::all();
      
        return view ('backend.user.create',compact('role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	 // dd($request);

        $user = new User();
     
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role_id = $request->role_id;
        $user->admin = 1;

        $user->password = Hash::make('password');
        $user->save();

        $userprofile = new UserProfile();
        $userprofile->user_id = $user->id;
        $userprofile->avatar = 'avatar.png';
        $userprofile->save();


        return redirect()->route('user.index')->with('flash_message', 'New user has been Added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $role = Role::all();
        $user = User::where('id',$id)->first();
        return view ('backend.user.edit',compact('role','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $user = User::findOrFail($id);

      
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role_id = $request->role_id;

        $user->userprofile->user_id = $user->id;
        $user->userprofile->avatar = 'avatar.png';
        $user->userprofile->save();

        $user->save();


        return redirect()->route('user.index')->with('flash_message', 'User has been Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $user->delete();

        return redirect()->back()->with('flash_message', 'User has been Deleted Successfully');
    }
}
