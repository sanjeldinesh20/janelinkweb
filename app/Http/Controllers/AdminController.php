<?php

namespace App\Http\Controllers;

use App\Mail\ProfileChange;
use App\Mail\PasswordChange;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Carbon;
use Session;
use File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Input;
use Image;
use App\Visitor;
use App\Ticketing;
use App\Partner;
use App\Employee;
use DB;


class AdminController extends Controller
{
    // Login
    public function login(Request $request){
        if($request->isMethod('post')){
            $data = $request->input();
            if(Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'admin' => '1'])){
                return redirect()->route('dashboard');
            } else {
                return redirect()->route('admin.login')->with('flash_message_error', 'Invalid Username and Password');
            }
        }
        if(Auth::guard()->check()){
            return redirect()->route('dashboard');
        } else {
            return view ('admin.login')->with('flash_message_error', 'Please Login to Access');
        }
    }

    // Dashboard

    /**
     * @return $this
     */
    public function dashboard(){



        $expenses_total = DB::table('expenses')
            ->select(DB::raw('SUM(expense_price) as total_expense,DATE(expense_date) as date,
          MONTH(expense_date) as month, YEAR(expense_date) as year'))
            ->groupBy(DB::raw('YEAR(expense_date) ASC, MONTH(expense_date) ASC, DATE(expense_date) ASC '))->latest()->get();

        $totalticket = DB::table('ticketings')
            ->select(DB::raw('SUM(expenses_cost) as total_ticketings,DATE(book_date) as date,
          MONTH(book_date) as month, YEAR(book_date) as year'))
            ->groupBy(DB::raw('YEAR(book_date) ASC, MONTH(book_date) ASC, DATE(book_date) ASC '))->latest()->get();


        $totalticketsell = DB::table('ticketings')
            ->select(DB::raw('SUM(selling_cost) as total_sell,DATE(book_date) as date,
          MONTH(book_date) as month, YEAR(book_date) as year'))
            ->groupBy(DB::raw('YEAR(book_date) ASC, MONTH(book_date) ASC, DATE(book_date) ASC '))->latest()->get();

//dd($totalticketsell);


        $count_ticketing = Ticketing::whereDate('created_at', Carbon::today())->count();

        $total_partner = Partner::count();
        $total_employee = Employee::count();

        $total_visitor = Visitor::count();
        return view ('admin.dashboard')->with(compact('total_visitor','count_ticketing','total_partner','total_employee','expenses_total','totalticket','totalticketsell'));
    }

    // Logout
    public function logout(){
        Session::flush();
        return redirect()->route('admin.login')->with('flash_message_success', 'Logout Successful');
    }

    // Admin profile
    public function profile($id){
     
        $user = Auth::user()->findOrFail($id);
      return view ('admin.profile', compact('user'));
    }

    //update profile
    public function updateProfile(Request $request,$id)
    {
        $user = User::where('id', $request->id)->first();

        //dd($request->all());
        $image_tmp = Input::file('avatar');

        if (!empty($image_tmp)){
            if ($image_tmp->isValid()) {
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(111, 99999) . '.' . $extension;
                $image_path = 'public/uploads/user/small/' . $filename;
                $small_image_path = 'public/uploads/user/small/' . $filename;

                //Resize Images
                Image::make($image_tmp)->save($image_path);
                Image::make($image_tmp)->resize(60,60)->save($small_image_path);

                
                $image_path = $data['current_avatar'];
                if(!empty($data['avatar'])){
                    if(File::exists($image_path)){
                        File::delete($image_path);
                    }   
                }
                $small_image_path = $data['current_avatar'];
                if(!empty($data['avatar'])){
                    if(File::exists($small_image_path)){
                        File::delete($small_image_path);
                    }   
                }
            }else{
                $filename = $request->current_avatar;
            }
        }else {
            $filename = $request->current_avatar;

        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->userprofile->phone = $request->phone;
        $user->userprofile->avatar = $filename;
        $user->userprofile->facebook = $request->facebook;
        $user->userprofile->twitter = $request->twitter;
        $user->userprofile->instagram = $request->instagram;
        $user->userprofile->linkedin = $request->linkedin;
        $user->userprofile->about = $request->about;
        $user->userprofile->save();
        $user->save();
        // Mail::to($request->email)->send(new ProfileChange($request->all()));
        
    return redirect()->back()->with('flash_message', 'Your Profile has been updated Successfully');
}


  // update password
  public function editPassword(Request $request){
        if ($request->isMethod('post')){
            $this->validate($request,[
                'current_password' => 'required',
                'new_password' => 'required',
                'confirm_password' => 'required'
            ]);

            $data = $request->all();

            $check_password = User::where(['email' => Auth::guard('web')->user()->email])->first();

            $current_password = $data['current_password'];
            if(Hash::check($current_password,$check_password->password))
            {
                $password = bcrypt($data['new_password']);
                User::where('email',Auth::guard('web')->user()->email)->update(['password'=>$password]);


                // Mail::to($request->email)->send(new PasswordChange($request->all()));


                return redirect()->back()->with('flash_message','Password updated Successfully!');
            }
            else {
                return redirect()->back()->with('danger','Incorrect Current Password!');
            }
        }
        return view ('admin.edit_password');
    }

    // Checking Current Admin Password
public function chkUserPassword(Request $request){
    $data = $request->all();
    $current_password = $data['current_password'];
    $user_id = Auth::guard('web')->user()->id;
    $check_password = User::where('id', $user_id)->first();
    if (Hash::check($current_password, $check_password->password)){
        echo "true"; die;
    }else{
        echo "false"; die;
    }
}
}
