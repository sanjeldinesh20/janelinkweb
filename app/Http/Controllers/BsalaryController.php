<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Bsalary;
use Illuminate\Http\Request;

class BsalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bsalary = Bsalary::all();
        return view ('backend.bsalary.index')->with('bsalary',$bsalary);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employee = Employee::all();
        return view ('backend.bsalary.create',compact('employee'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $this->validate($request,[
            'employee_id' => 'required|unique:bsalaries',
        ],
            [

                'employee_id.unique' => ' employee   name is already taken.',


            ]);
        $bsalary = new Bsalary();


        $bsalary->employee_id = $request->employee_id;

        $bsalary->basic_amount = $request->basic_amount;

        $bsalary->description = $request->description;



        $bsalary->save();

        return redirect()->route('basic.index')->with('flash_message', 'New Basic salary has been Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::all();
        $bsalary = Bsalary::where('id',$id)->first();
        return view ('backend.bsalary.edit',compact('bsalary','employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bsalary = Bsalary::findOrFail($id);

        $bsalary->employee_id = $request->employee_id;

        $bsalary->basic_amount = $request->basic_amount;

        $bsalary->description = $request->description;


        $bsalary->save();

        return redirect()->route('basic.index')->with('flash_message', 'Basic Salary has been Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bsalary = Bsalary::find($id);

        $bsalary->delete();

        return redirect()->back()->with('flash_message', 'Basic Salary has been Deleted Successfully');
    }
}
