<?php

namespace App\Http\Controllers;

use App\Airport;
use Illuminate\Http\Request;
use App\Service;
use App\Country;
use App\Ticketing;
use App\Visitor;
use App\Employee;
use DB;
use Illuminate\Support\Facades\Input;

class TicketingController extends Controller
{
    public function index()
    {
        $ticketing = Ticketing::all();

        return view ('backend.ticket.index')->with('ticketing',$ticketing);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        $service = Service::all();
        $country = Country::all();
        $employee = Employee::all();
        $airports = DB::select('select countryName from airports');
       // dd($airports);
        return view ('backend.ticket.create')->with('service',$service)->with('country',$country)->with('employee',$employee)->with('airports',$airports);

    }


     

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $ticketing = new Ticketing();

         $fullname[] = $request->input('fullname');
            $fname = json_encode($fullname);


            $gender[] = $request->input('gender');
            $gender_name = json_encode($gender);


             $age[] = $request->input('age');
            $age_name = json_encode($age);

             $passportno[] = $request->input('passportno');
            $passport_name = json_encode($passportno);

            $phoneno[] = $request->input('phoneno');
            $phone_name = json_encode($phoneno);

             $email[] = $request->input('email');
            $email_name = json_encode($email);



              $flightnumber[] = $request->input('flightnumber');
            $flight_name = json_encode($flightnumber);


             $fromto[] = $request->input('fromto');
            $from_name = json_encode($fromto);


            $flightdate[] = $request->input('flightdate');
            $date_name = json_encode($flightdate);



             $timeofdeparture[] = $request->input('timeofdeparture');
            $time_name = json_encode($timeofdeparture);

             $arrivaltime[] = $request->input('arrivaltime');
            $arrival_name = json_encode($arrivaltime);


            $sts[] = $request->input('sts');
            $sts_name = json_encode($sts);



       

        $ticketing->way_type = $request->way_type;
        $ticketing->book_date = $request->book_date;
        $ticketing->service_id = $request->service_id;
        $ticketing->full_name = $request->full_name;
        $ticketing->flight_date = $request->flight_date;
        $ticketing->flight_time = $request->flight_time;
        $ticketing->fromcountryName = $request->fromcountryName;
        $ticketing->fromairportName = $request->fromairportName;
        $ticketing->tocountryName = $request->tocountryName;
        $ticketing->toairportName = $request->toairportName;
        $ticketing->number_passenger = $request->number_passenger;
        $ticketing->fullname = $fname;
        $ticketing->gender = $gender_name;
        $ticketing->age = $age_name;
        $ticketing->passportno = $passport_name;
        $ticketing->phoneno = $phone_name;
        $ticketing->email = $email_name;
        $ticketing->number_kids = $request->number_kids;
        $ticketing->arilines = $request->arilines;
        $ticketing->flightnumber = $flight_name;
        $ticketing->fromto = $from_name;
        $ticketing->flightdate = $date_name;
        $ticketing->timeofdeparture = $time_name;
        $ticketing->arrivaltime = $arrival_name;
        $ticketing->sts = $sts_name;
        $ticketing->expenses_cost = $request->expenses_cost;
        $ticketing->selling_cost = $request->selling_cost;
        $ticketing->additional_cost = $request->additional_cost;
        $ticketing->employee_id = $request->employee_id;
        $ticketing->description = $request->description;
      



           
        $ticketing->save();

        return redirect()->route('ticket.print',$ticketing->id)->with('flash_message', 'New ticketing has been Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $ticketing = Ticketing::where('id',$id)->first();
        return view ('backend.ticket.show',compact('ticketing'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   $service = Service::all();
        $country = Country::all();
        $employee = Employee::all();
        $airports = DB::select('select countryName from airports');


        $ticketing = Ticketing::where('id',$id)->first();
        return view ('backend.ticket.edit',compact('ticketing','service','country','airports','employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ticketing = Ticketing::findOrFail($id);

        $fullname[] = $request->input('fullname');
        $fname = json_encode($fullname);


        $gender[] = $request->input('gender');
        $gender_name = json_encode($gender);


        $age[] = $request->input('age');
        $age_name = json_encode($age);

        $passportno[] = $request->input('passportno');
        $passport_name = json_encode($passportno);

        $phoneno[] = $request->input('phoneno');
        $phone_name = json_encode($phoneno);

        $email[] = $request->input('email');
        $email_name = json_encode($email);



        $flightnumber[] = $request->input('flightnumber');
        $flight_name = json_encode($flightnumber);


        $fromto[] = $request->input('fromto');
        $from_name = json_encode($fromto);


        $flightdate[] = $request->input('flightdate');
        $date_name = json_encode($flightdate);



        $timeofdeparture[] = $request->input('timeofdeparture');
        $time_name = json_encode($timeofdeparture);

        $arrivaltime[] = $request->input('arrivaltime');
        $arrival_name = json_encode($arrivaltime);


        $sts[] = $request->input('sts');
        $sts_name = json_encode($sts);





        $ticketing->way_type = $request->way_type;
        $ticketing->book_date = $request->book_date;
        $ticketing->service_id = $request->service_id;
        $ticketing->full_name = $request->full_name;
        $ticketing->flight_date = $request->flight_date;
        $ticketing->flight_time = $request->flight_time;
        $ticketing->fromcountryName = $request->fromcountryName;
        $ticketing->fromairportName = $request->fromairportName;
        $ticketing->tocountryName = $request->tocountryName;
        $ticketing->toairportName = $request->toairportName;
        $ticketing->number_passenger = $request->number_passenger;
        $ticketing->fullname = $fname;
        $ticketing->gender = $gender_name;
        $ticketing->age = $age_name;
        $ticketing->passportno = $passport_name;
        $ticketing->phoneno = $phone_name;
        $ticketing->email = $email_name;
        $ticketing->number_kids = $request->number_kids;
        $ticketing->arilines = $request->arilines;
        $ticketing->flightnumber = $flight_name;
        $ticketing->fromto = $from_name;
        $ticketing->flightdate = $date_name;
        $ticketing->timeofdeparture = $time_name;
        $ticketing->arrivaltime = $arrival_name;
        $ticketing->sts = $sts_name;
        $ticketing->expenses_cost = $request->expenses_cost;
        $ticketing->selling_cost = $request->selling_cost;
        $ticketing->additional_cost = $request->additional_cost;
        $ticketing->employee_id = $request->employee_id;
        $ticketing->description = $request->description;

       

        $ticketing->save();

        return redirect()->route('ticket.index')->with('flash_message', 'ticketing has been Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $ticketing = Ticketing::find($id);
                
        $ticketing->delete();

        return redirect()->back()->with('flash_message', 'ticketing has been Deleted Successfully');
    }

     public function print($id)
    {
        $ticketed = Ticketing::where('id',$id)->first();
      //  dd($ticketing);
        return view ('backend.ticket.print',compact('ticketed'));
    }

    function fetch(Request $request)
    {
//      dd($request);
            $query = $request->get('query');
            //dd($query);

        $data = Visitor::where('visitor_name', 'LIKE', "$query%")->get();

//dd($data);
            $output = '<ul class="dropdown-menu" style="display:block; position:relative">';

            foreach($data as $row)
            {
                $output .= '
       <li><a href="#">'.$row->visitor_name.'</a></li>
       ';
            }
            $output .= '</ul>';
            echo $output;

    }


    public function findcountry(Request $r)
    {
        $airports =Airport::where('countryName' ,'Like', $r->fromcountryid)->distinct('name')->get();
//        $data = DB::SELECT * FROM 'airports' WHERE countryName = $r->fromcountryid;
//dd($data);
        return view('backend.ticket.ajax.airport_list')->with('airports',$airports);
    }


    public function findcountryto(Request $r)
    {
        $airports =Airport::where('countryName' ,'Like', $r->tocountryid)->distinct('name')->get();
//        $data = DB::SELECT * FROM 'airports' WHERE countryName = $r->fromcountryid;
//dd($data);
        return view('backend.ticket.ajax.airport_list_to')->with('airports',$airports);
    }

    public function report(Request $r)
    {



        if ($r->isMethod('post')){
            //dd($r);
            $start = Input::get ( 'firstdate' );
            $end = Input::get ( 'lastdate' );
//dd($end);
            $ticketing = Ticketing::whereBetween('book_date', [$start, $end])->get();

             dd($ticketing);

            $search = 1;
            return view('backend.ticket.report', compact('expenses','search','start','end'));

        }
        $search = 0;

        return view('backend.ticket.report')->with(compact('search'));
    }

}
