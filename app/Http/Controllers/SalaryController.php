<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use DB;
use App\Salary;

class SalaryController extends Controller
{

 public function index()
    {
        $salary = Salary::all();
        return view ('backend.salary.index')->with('salary',$salary);
    }

     public function create()
    {   $employee = Employee::all();
        $months = DB::select('select * from months');
         return view ('backend.salary.create',compact('months','employee'));

    }

     public function store(Request $request)
    {


        $salary = new Salary();

        
        $salary->employee_id = $request->employee_id;
        $salary->month_id = $request->month_id;
        $salary->basic_salary = $request->basic_salary;
        $salary->advance_id = $request->advance_id;
        $salary->leave_taken = $request->leave_taken;


        $salary->description = $request->description;
       

      

       
        $salary->save();

        return redirect()->route('salary.print',$salary->id)->with('flash_message', 'New Salary has been Added Successfully');
    }

     public function edit($id)
    {    $employee = Employee::all();
        $months = DB::select('select * from month');
        $salary = Salary::where('id',$id)->first();
        return view ('backend.salary.edit',compact('salary','months','employee'));
    }

    public function update(Request $request, $id)
    {
        
        $salary = Salary::findOrFail($id);

        $salary->employee_id = $request->employee_id;
        $salary->month_id = $request->month_id;
        $salary->basic_salary = $request->basic_salary;
        $salary->advance_id = $request->advance_id;
        $salary->leave_taken = $request->leave_taken;

        $salary->description = $request->description;

       

        $salary->save();

        return redirect()->route('salary.print')->with('flash_message', 'Salary has been Updated Successfully');
    }

     public function destroy($id)
    {
       $salary = Salary::find($id);
                
        $salary->delete();

        return redirect()->back()->with('flash_message', 'salary has been Deleted Successfully');
    }
  public function findadvance(Request $r)
    {
      
       $data = DB::select("SELECT * FROM `advances` WHERE employee_id = ".$r->employee_id);
       return $data;
    }

    public function findbasic(Request $r)
    {

        $data = DB::select("SELECT * FROM `bsalaries` WHERE employee_id = ".$r->employee_id);
        return $data;
    }

    public function print($id)
    {
        $salary = Salary::where('id',$id)->first();
        return view ('backend.salary.print',compact('salary'));
    }
}
