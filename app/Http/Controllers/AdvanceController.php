<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Advance;
use App\Employee;

class AdvanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $advance = Advance::all();
        return view ('backend.advance.index')->with('advance',$advance);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   $employee = Employee::all();
        $months = DB::select('select * from months');
       
        return view ('backend.advance.create',compact('months','employee'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $advance = new Advance();

        
        $advance->employee_id = $request->employee_id;
        $advance->month_id = $request->month_id;
        $advance->advance_amount = $request->advance_amount;
        $advance->advanced_date = $request->advanced_date;
        $advance->description = $request->description;
      

       
        $advance->save();

        return redirect()->route('advance.index')->with('flash_message', 'New advance has been Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {    $employee = Employee::all();
        $months = DB::select('select * from months');
        $advance = advance::where('id',$id)->first();
        return view ('backend.advance.edit',compact('advance','months','employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $advance = Advance::findOrFail($id);

        $advance->employee_id = $request->employee_id;
        $advance->month_id = $request->month_id;
        $advance->advance_amount = $request->advance_amount;
        $advance->advanced_date = $request->advanced_date;
        $advance->description = $request->description;

       

        $advance->save();

        return redirect()->route('advance.index')->with('flash_message', 'advance has been Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $advance = Advance::find($id);
                
        $advance->delete();

        return redirect()->back()->with('flash_message', 'advance has been Deleted Successfully');
    }

    
}
