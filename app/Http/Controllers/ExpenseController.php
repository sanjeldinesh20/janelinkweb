<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
use App\Employee;
use DB;
class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $expenses = Expense::all();
        return view ('backend.expense.index')->with('expenses',$expenses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employee = Employee::all();
        return view ('backend.expense.create')->with('employee',$employee);
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//dd( $request->expense_date);
         $expenses = new Expense();

        $expenses->expense_date = $request->expense_date;
        $expenses->expense_title = $request->expense_title;
        $expenses->expense_price = $request->expense_price;
        $expenses->employee_id = $request->employee_id;        
        $expenses->description = $request->description;
       

       
        $expenses->save();

        return redirect()->route('expense.index')->with('flash_message', 'New expenses has been Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   $employee = Employee::all();
        $expenses = Expense::where('id',$id)->first();
        return view ('backend.expense.edit',compact('expenses','employee')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $expenses = Expense::findOrFail($id);

        $expenses->expense_date = $request->expense_date;
        $expenses->expense_title = $request->expense_title;
        $expenses->expense_price = $request->expense_price;
        $expenses->employee_id = $request->employee_id;        
        $expenses->description = $request->description;
       

       
        $expenses->save();

        return redirect()->route('expense.index')->with('flash_message', ' Expenses has been Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expenses = Expense::find($id);


        $expenses->delete();

        return redirect()->back()->with('flash_message', 'expenses has been Deleted Successfully');
    }



    public static function totalExpensesByMonth(){
        $expenses = DB::table('expenses')
            ->select(DB::raw('SUM(expense_price) as total_expense, MONTH(date) as month, YEAR(date) as year'))
            ->groupBy(DB::raw('YEAR(date) ASC, MONTH(date) ASC'))->get();

        return $expenses;
    }


}
