<?php

namespace App\Http\Controllers;

use App\Visitor;
use Illuminate\Http\Request;
use App\Expense;
use App\Hotel;
use App\Ticketing;
use DB;
use PDF;
use Illuminate\Support\Facades\Input;

class ReportController extends Controller
{
    public function index(Request $request)
    {
//        $expenses = Expense::select(DB::raw('SUM(expense_price) as total_expense'),'expense_title')
//            ->groupBy(DB::raw('DATE(expense_date) ASC '))->get();
//        dd($expenses);
        return view('backend.report.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filter(Request $request)
    {
       // dd($request);
        $date_filter = $request->date_filter;

  //dd($request->date);

       /* if (!$date_filter)
            $date_filter = 'day';*/

//        $expenses = null;
        if ($date_filter === 'day') {

            $start = Input::get ( 'date' );
            $end = Input::get ( 'date' );
//dd($end);
            $data = Expense::whereBetween('expense_date', [$start, $end])->get();

           dd($data);
            return view('backend.report.ajax.ajax', compact('data'));
        }

        if ($date_filter === 'month') {
            $data = Expense::addSelect(DB::raw('SUM(expense_price) as total_expense','expense_title','employee_id'))
                ->groupBy(DB::raw(' MONTH(expense_date) ASC'))->latest()->get();
            dd($data);
        }
        if ($date_filter === 'year'){
            $data = Expense::addSelect(DB::raw('SUM(expense_price) as total_expense','expense_title','employee_id'))
                ->groupBy(DB::raw('YEAR(expense_date) ASC'))->latest()->get();
            dd($data);

        }

        return view('backend.report.ajax.ajax', compact('data'));
    }




    public function generatePDF(Request $request)
    {

         $data = ['title' => 'Welcome to HDTuto.com'];

        $pdf = PDF::loadView('backend.report.pdf', $data);
  

//return view('backend.report.pdf');

       return $pdf->download('Janelink Travel.pdf');



    }


    public function booking(Request $request)
    {

        $hotel = DB::table('hotels')
        ->select(DB::raw('SUM(expenses_cost) as total_expense,SUM(selling_cost) as total_selling,
         SUM(additional_cost) as total_additional,
         (GROUP_CONCAT(hotels.full_name)) as names,DATE(book_date) as date,
          MONTH(book_date) as month, YEAR(book_date) as year'))
         ->groupBy(DB::raw('YEAR(book_date) ASC, MONTH(book_date) ASC, DATE(book_date) ASC '))->latest()->get();
 //dd($hotel);

         $pdf = PDF::loadView('backend.report.hotelbookingpdf',compact('hotel'));

        return $pdf->download('Hotel Booking PDF.pdf');

       // return view('backend.report.hotelbookingpdf', compact('hotel'));

    }

    public function ticketbooking(Request $request)
    {

        $ticket = DB::table('ticketings')
            ->select(DB::raw('SUM(expenses_cost) as total_expense,SUM(selling_cost) as total_selling,
         SUM(additional_cost) as total_additional,(SUM(expenses_cost)+SUM(selling_cost)+SUM(additional_cost)) as gtotal,
         (GROUP_CONCAT(ticketings.full_name)) as names,DATE(book_date) as date,
          MONTH(book_date) as month, YEAR(book_date) as year'))
            ->groupBy(DB::raw('YEAR(book_date) ASC, MONTH(book_date) ASC, DATE(book_date) ASC '))->latest()->get();

       // dd($ticket);


        $pdf = PDF::loadView('backend.report.ticketbookingpdf',compact('ticket'));

      //  return $pdf->download('Ticket Booking PDF.pdf');

         return view('backend.report.ticketbookingpdf', compact('ticket'));

    }


    public function findreport(Request $r)
    {
//
        if ($r->isMethod('post')){
          //  dd($r);
            $start = Input::get ( 'firstdate' );
            $end = Input::get ( 'lastdate' );
//dd($end);
            $expenses = Expense::whereBetween('expense_date', [$start, $end])->get();

           // dd($expenses);

            $search = 1;
            return view('backend.report.search', compact('expenses','search','start','end'));

        }
        $search = 0;

        return view('backend.report.search')->with(compact('search'));


    }


//    public function totalprojects()
//    {
//        $total_visitor = Visitor::count();
//        return view('admin.dashboard')->with(['$total_visit'=>$total_visitor]);
//    }
}
