<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\SiteSetting;
use Session;
use Auth;
use File;
use Image;
use Illuminate\Support\Facades\Input;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $employee = Employee::all();
        return view ('backend.employee.index')->with('employee',$employee);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
        return view ('backend.employee.create');
    }


        public function store(Request $request)
    {


        $employee = new Employee();

         $image_tmp = Input::file('employee_image');
        
        if ($image_tmp->isValid()) {
            $extension = $image_tmp->getClientOriginalExtension();
            $filename = rand(111,99999).'.'.$extension;
            
            $extra_small_image_path = 'public/uploads/employee/verysmall/'.$filename;
            $image_path = 'public/uploads/employee/'.$filename;

            //Resize Images            
            Image::make($image_tmp)->resize(80,90)->save($extra_small_image_path);
            Image::make($image_tmp)->save($image_path);
        }
        
        $employee->employee_name = $request->employee_name;
        $employee->age = $request->age;
        $employee->dob = $request->dob;
        $employee->employee_image = $filename;
        $employee->gender = $request->gender;
        $employee->employee_address = $request->employee_address;
        $employee->employee_email = $request->employee_email;
        $employee->employee_phone = $request->employee_phone;
        $employee->employee_passport = $request->employee_passport;
        $employee->hired_date = $request->hired_date;  
        $employee->description = $request->description;

       
        $employee->save();

        return redirect()->route('employee.index')->with('flash_message', 'New employee has been Added Successfully');

    }


     public function edit($id)
    {
       
        $employee = employee::where('id',$id)->first();
        return view ('backend.employee.edit',compact('employee'));
    }


     public function update(Request $request, $id)
    {
        // dd($request);
        $employee = employee::findOrFail($id);

        $image_tmp = Input::file('employee_image');

        if (!empty($image_tmp)){
            if ($image_tmp->isValid()) {
                $extension = $image_tmp->getClientOriginalExtension();
                $filename_new = rand(111, 99999) . '.' . $extension;

               
                $extra_small_image_path = 'public/uploads/employee/verysmall/'.$filename_new;
                $image_path = 'public/uploads/employee/'.$filename_new;
    
                //Resize Images
                
                Image::make($image_tmp)->resize(80,90)->save($extra_small_image_path);
                Image::make($image_tmp)->save($image_path);

              
                $extra_small_image_path = 'public/uploads/employee/verysmall/';
                $image_path = 'public/uploads/employee/';
                
              
               
                if(!empty($request->employee_image))
                {
                    if(file_exists($extra_small_image_path.$request->current_image ))
                    {
                        unlink($extra_small_image_path.$request->current_image );
                    }
                }
                if(!empty($request->employee_image))
                {
                    if(file_exists($image_path.$request->current_image ))
                    {
                        unlink($image_path.$request->current_image );
                    }
                }

            }else{
                $filename_new = $request->current_image;
            }

        }
        else {
            $filename_new = $request->current_image;

        }

       $employee->employee_name = $request->employee_name;
        $employee->age = $request->age;
        $employee->dob = $request->dob;
       $employee->employee_image = $filename_new;        
        $employee->gender = $request->gender;
        $employee->employee_address = $request->employee_address;
        $employee->employee_email = $request->employee_email;
        $employee->employee_phone = $request->employee_phone;
        $employee->employee_passport = $request->employee_passport;
        $employee->hired_date = $request->hired_date;
        $employee->description = $request->description;

       

        $employee->save();

        return redirect()->route('employee.index')->with('flash_message', 'employee has been Updated Successfully');
    }

     public function destroy($id)
    {
        $employee = employee::find($id);

        
        

        $employee->delete();

        return redirect()->back()->with('flash_message', 'employee has been Deleted Successfully');
    }
}
