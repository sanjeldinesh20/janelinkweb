<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Nationality;
use App\Advertisement;
use App\SmallAdvertisement;
use App\MediumAdvertisement;
use App\LargeAdvertisement;
use App\Blog;
use App\Thought;
use App\SiteSetting;
use App\NewsReview;
use App\Mail\ContactUsMail;
use Illuminate\Support\Facades\Mail;
use App\News;
use App\Interview;
use Session;

class FrontendController extends Controller
{
    public function index()
    {   
        


        return view ('frontend.index');
    }
  
    public function sendcontactmail(Request $request)
    {
        if($request->isMethod('post')){
            Mail::to('onlinezealtest@gmail.com')->send(new ContactUsMail($request->all()));

        }
        return redirect()->back();
    }

    

}
