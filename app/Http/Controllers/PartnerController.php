<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nationality;
use App\Partner;
use Session;
use Auth;
use File;
use Image;
use Illuminate\Support\Facades\Input;
use App\Service;
class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partner = Partner::latest()->get();
        return view ('backend.partner.index',compact('partner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
        $service = Service::all();
        return view ('backend.partner.create')->with('service',$service);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $partner = new Partner();

        $image_tmp = Input::file('partner_image');
        


        $partner->partner_name = $request->partner_name;
        $partner->partner_email = $request->partner_email;
        $partner->partner_address = $request->partner_address;
        $partner->partner_contact = $request->partner_contact;        
        $partner->service_id = $request->service_id;
        $partner->partner_commission = $request->partner_commission;
        $partner->description = $request->description;
       
        $partner->save();

        return redirect()->route('partner.index')->with('flash_message', 'New Partner has been Added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $partner = partner::where('id',$id)->first();
        return view ('backend.partner.show',compact('partner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::all();
      
        $partner = Partner::where('id',$id)->first();
        return view ('backend.partner.edit',compact('partner','service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $partner = partner::findOrFail($id);

        $image_tmp = Input::file('partner_image');

        if (!empty($image_tmp)){
            if ($image_tmp->isValid()) {
                $extension = $image_tmp->getClientOriginalExtension();
                $filename_new = rand(111, 99999) . '.' . $extension;

               
                $extra_small_image_path = 'public/uploads/partner/verysmall/'.$filename_new;
                $image_path = 'public/uploads/partner/'.$filename_new;
    
                //Resize Images
                
                Image::make($image_tmp)->resize(80,90)->save($extra_small_image_path);
                Image::make($image_tmp)->save($image_path);

              
                $extra_small_image_path = 'public/uploads/partner/verysmall/';
                $image_path = 'public/uploads/partner/';
                
              
               
                if(!empty($request->partner_image))
                {
                    if(file_exists($extra_small_image_path.$request->current_image ))
                    {
                        unlink($extra_small_image_path.$request->current_image );
                    }
                }
                if(!empty($request->partner_image))
                {
                    if(file_exists($image_path.$request->current_image ))
                    {
                        unlink($image_path.$request->current_image );
                    }
                }

            }else{
                $filename_new = $request->current_image;
            }

        }
        else {
            $filename_new = $request->current_image;

        }

        $partner->partner_name = $request->partner_name;
        $partner->partner_image = $filename_new;        
        $partner->partner_email = $request->partner_email;
        $partner->partner_address = $request->partner_address;
        $partner->partner_contact = $request->partner_contact;        
        $partner->service_id = $request->service_id;
        $partner->partner_commission = $request->partner_commission;
        $partner->description = $request->description;
      

        $partner->save();

        return redirect()->route('partner.index')->with('flash_message', 'partner has been Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner = Partner::find($id);

        
        

        $partner->delete();

        return redirect()->back()->with('flash_message', 'partner has been Deleted Successfully');
    }
}
