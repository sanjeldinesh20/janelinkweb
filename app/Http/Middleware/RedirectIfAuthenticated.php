<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if (Auth::guard($guard)->check()) {
        //   return redirect()->action('AdminController@login')->with('flash_message_error', 'Please Login to Access');
        // } else {
        //   return redirect('/admin/dashboard');
        // }
        // return $next($request);

        switch($guard){
          case 'web':
          if(Auth::guard($guard)->check()){
            return redirect()->route('dashboard');
          }
          break;

          default:
          if(Auth::guard($guard)->check()){
            return redirect()->route('admin.login');
          }
          break;
        }
        return $next($request);
    }
}
