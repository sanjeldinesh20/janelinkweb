<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model

{
    protected $appends = ['total'];

    public function employee()
    { 
    	return $this->belongsTo('App\Employee');

    }

public function month()
    { 
    	return $this->belongsTo('App\Month');

    }
    public function getTotalAttribute()
    {
        return $this->basic_salary + $this->advance_id;
    }

}
