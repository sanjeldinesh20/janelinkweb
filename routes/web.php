<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/','FrontendController@index')->name('index');

Route::post('/contact-us-mail','FrontendController@sendcontactmail')->name('sendcontactmail');

Route::post('/news-details/store-review','FrontendController@newsReview')->name('newsReview');

Auth::routes();

Route::match(['get', 'post'], '/admin-login', 'AdminController@login')->name('admin.login');

//Password reset routes
Route::post('admin/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('admin/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('admin/password/reset', 'Auth\ResetPasswordController@reset')->name('admin.password.update');
Route::get('admin/password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');

Route::group(['middleware' => 'auth'], function(){
    
    Route::get('/admin/dashboard', 'AdminController@dashboard')->name('dashboard');
    Route::get('/admin/profile/{id}', 'AdminController@profile')->name('profile');
    Route::post('/admin/update/profile/{id}', 'AdminController@updateProfile')->name('update.profile');
    Route::match(['get', 'post'], '/admin/edit/password', 'AdminController@editPassword')->name('edit.password');
    Route::post('/admin/edit/check-password', 'AdminController@chkUserPassword');

    Route::get('/admin/site-setting/{id}','SiteSettingController@edit')->name('sitesetting.edit');
    Route::post('/admin/site-setting/{id}','SiteSettingController@update')->name('sitesetting.update');

    Route::get('/admin/add-partner','PartnerController@create')->name('partner.create');
    Route::post('/admin/add-partner/store','PartnerController@store')->name('partner.store');
    Route::get('/admin/view-partner','PartnerController@index')->name('partner.index');
    Route::get('/admin/show-partner/{id}','PartnerController@show')->name('partner.show');
    Route::get('/admin/edit-partner/{id}','PartnerController@edit')->name('partner.edit');
    Route::post('/admin/edit-partner/update/{id}','PartnerController@update')->name('partner.update');
    Route::get('/admin/partner/destroy/{id}','PartnerController@destroy')->name('partner.destroy');


     Route::get('/admin/add-visitor','VisitorController@create')->name('visitor.create');
    Route::post('/admin/add-visitor/store','VisitorController@store')->name('visitor.store');
    Route::get('/admin/view-visitor','VisitorController@index')->name('visitor.index');
    Route::get('/admin/show-visitor/{id}','VisitorController@show')->name('visitor.show');
    Route::get('/admin/edit-visitor/{id}','VisitorController@edit')->name('visitor.edit');
    Route::post('/admin/edit-visitor/update/{id}','VisitorController@update')->name('visitor.update');
    Route::get('/admin/visitor/destroy/{id}','VisitorController@destroy')->name('visitor.destroy');



    Route::get('/admin/add-service','ServiceController@create')->name('service.create');
    Route::post('/admin/add-service/store','ServiceController@store')->name('service.store');
    Route::get('/admin/view-service','ServiceController@index')->name('service.index');
    Route::get('/admin/show-service/{id}','ServiceController@show')->name('service.show');
    Route::get('/admin/edit-service/{id}','ServiceController@edit')->name('service.edit');
    Route::post('/admin/edit-service/update/{id}','ServiceController@update')->name('service.update');
    Route::get('/admin/service/destroy/{id}','ServiceController@destroy')->name('service.destroy');

    Route::get('/admin/add-employee','EmployeeController@create')->name('employee.create');
    Route::post('/admin/add-employee/store','EmployeeController@store')->name('employee.store');
    Route::get('/admin/view-employee','EmployeeController@index')->name('employee.index');
    Route::get('/admin/show-employee/{id}','EmployeeController@show')->name('employee.show');
    Route::get('/admin/edit-employee/{id}','EmployeeController@edit')->name('employee.edit');
    Route::post('/admin/edit-employee/update/{id}','EmployeeController@update')->name('employee.update');
    Route::get('/admin/employee/destroy/{id}','EmployeeController@destroy')->name('employee.destroy');

    Route::get('/admin/add-expenses','ExpenseController@create')->name('expense.create');
    Route::post('/admin/add-expenses/store','ExpenseController@store')->name('expense.store');
    Route::get('/admin/view-expenses','ExpenseController@index')->name('expense.index');
    Route::get('/admin/show-expenses/{id}','ExpenseController@show')->name('expense.show');
    Route::get('/admin/edit-expenses/{id}','ExpenseController@edit')->name('expense.edit');
    Route::post('/admin/edit-expenses/update/{id}','ExpenseController@update')->name('expense.update');
    Route::get('/admin/expense/destroy/{id}','ExpenseController@destroy')->name('expense.destroy');


    Route::get('/admin/add-salary','SalaryController@create')->name('salary.create');
    Route::post('/admin/add-salary/store','SalaryController@store')->name('salary.store');
    Route::get('/admin/view-salary','SalaryController@index')->name('salary.index');
    Route::get('/admin/show-salary/{id}','SalaryController@show')->name('salary.show');
    Route::get('/admin/edit-salary/{id}','SalaryController@edit')->name('salary.edit');
    Route::post('/admin/edit-salary/update/{id}','SalaryController@update')->name('salary.update');
    Route::get('/admin/salary/destroy/{id}','SalaryController@destroy')->name('salary.destroy');
    Route::get('/admin/printsalary/{id}','SalaryController@print')->name('salary.print');


    Route::get('/admin/add-basic-salary','BsalaryController@create')->name('basic.create');
    Route::post('/admin/add-basic-salary/store','BsalaryController@store')->name('basic.store');
    Route::get('/admin/view-basic-salary','BsalaryController@index')->name('basic.index');
    Route::get('/admin/show-basic-salary/{id}','BsalaryController@show')->name('basic.show');
    Route::get('/admin/edit-basic-salary/{id}','BsalaryController@edit')->name('basic.edit');
    Route::post('/admin/edit-basic-salary/update/{id}','BsalaryController@update')->name('basic.update');
    Route::get('/admin/basicsalary/destroy/{id}','BsalaryController@destroy')->name('basic.destroy');


    Route::get('/admin/add-advance','AdvanceController@create')->name('advance.create');
    Route::post('/admin/add-advance/store','AdvanceController@store')->name('advance.store');
    Route::get('/admin/view-advance','AdvanceController@index')->name('advance.index');
    Route::get('/admin/show-advance/{id}','AdvanceController@show')->name('advance.show');
    Route::get('/admin/edit-advance/{id}','AdvanceController@edit')->name('advance.edit');
    Route::post('/admin/edit-advance/update/{id}','AdvanceController@update')->name('advance.update');
    Route::get('/admin/advance/destroy/{id}','AdvanceController@destroy')->name('advance.destroy');



     Route::get('/admin/add-hotel-booking','HotelController@create')->name('hotel.create');
    Route::post('/admin/add-hotel-booking/store','HotelController@store')->name('hotel.store');
    Route::get('/admin/view-hotel-booking','HotelController@index')->name('hotel.index');
    Route::get('/admin/show-hotel-booking/{id}','HotelController@show')->name('hotel.show');
    Route::get('/admin/printhotel/{id}','HotelController@print')->name('hotel.print');    
    Route::get('/admin/edit-hotel-booking/{id}','HotelController@edit')->name('hotel.edit');
    Route::post('/admin/edit-hotel-booking/update/{id}','HotelController@update')->name('hotel.update');
    Route::get('/admin/hotel/destroy/{id}','HotelController@destroy')->name('hotel.destroy');



    Route::get('/admin/add-ticket-booking','TicketingController@create')->name('ticket.create');
    Route::post('/admin/add-ticket-booking/store','TicketingController@store')->name('ticket.store');
    Route::get('/admin/view-ticket-booking','TicketingController@index')->name('ticket.index');
    Route::get('/admin/show-ticket-booking/{id}','TicketingController@show')->name('ticket.show');
    Route::get('/admin/printticket/{id}','TicketingController@print')->name('ticket.print');
    Route::match(['get', 'post'], '/findticketreport','TicketingController@report')->name('ticket.report');


    Route::get('/admin/edit-ticket-booking/{id}','TicketingController@edit')->name('ticket.edit');
    Route::post('/admin/edit-ticket-booking/update/{id}','TicketingController@update')->name('ticket.update');
    Route::get('/admin/ticket/destroy/{id}','TicketingController@destroy')->name('ticket.destroy');
    Route::get('/findcountry','TicketingController@findcountry');
    Route::get('/findcountryto','TicketingController@findcountryto');


    Route::get('/admin/view-report','ReportController@index')->name('report.index');
    Route::get('/filterexpe','ReportController@filter')->name('report.filter');
//    Route::get('/admin/search-report','ReportController@search')->name('report.search');
    Route::get('/generate-pdf','ReportController@generatePDF')->name('report.generate');

    Route::get('/hotel-booking-report','ReportController@booking')->name('report.booking');
    Route::get('/ticket-booking-report','ReportController@ticketbooking')->name('report.ticket');

    Route::get('/findadvance','SalaryController@findadvance');
    Route::get('/findbasic','SalaryController@findbasic');

    Route::match(['get', 'post'], '/findreport','ReportController@findreport')->name('day.report');

    Route::post('/autocomplete/fetch', 'TicketingController@fetch')->name('autocomplete.fetch');




});

    


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', 'AdminController@logout')->name('admin.logout');


