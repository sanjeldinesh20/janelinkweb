<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserProfile;
use App\Module;
use App\Submodule;
use App\Accesslevel;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $user = new User();
        $user->name = 'Dinesh Sanjel';
        $user->email = 'dsanjel20@gmail.com';
        $user->password = Hash::make('password');
        $user->admin = 1;
       
        $user->save();
        
        $userprofile = new UserProfile();
        $userprofile->user_id = $user->id;
        $userprofile->avatar = 'avatar.png';
        $userprofile->phone = '9849668602';
        $userprofile->facebook = 'https://www.facebook.com/';
        $userprofile->twitter = 'https://twitter.com/';
        $userprofile->instagram = 'https://www.instagram.com/';
        $userprofile->linkedin = 'https://www.linkedin.com/feed/';
        $userprofile->about = "About Dinesh Sanjel";
        $userprofile->save();

        




    }
}
