<?php

use Illuminate\Database\Seeder;
use App\SiteSetting;

class SiteSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sites = new SiteSetting();

        $sites->organization_name = 'Jane Link';
        $sites->organization_logo = 'logo.png';       
        $sites->organization_email = ' info@janelink.com';
        $sites->contact_number = ' 987456320';
        $sites->organization_address = 'Japan';
        $sites->chief = 'Rajan ';
        $sites->director = 'Ramesh';
        $sites->tax = '13';
        $sites->organization_facebook = 'https://www.facebook.com/';
        $sites->organization_twitter = 'https://twitter.com/';
        $sites->organization_instagram = 'https://www.instagram.com/';
        $sites->organization_linkedin = 'https://www.linkedin.com/';

        $sites->save();
    }
}
