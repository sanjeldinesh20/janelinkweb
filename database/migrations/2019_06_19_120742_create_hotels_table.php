<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('id');
             $table->string('book_date')->nullable();
            $table->string('service_id')->nullable();
            $table->string('full_name')->nullable();
            $table->string('age')->nullable();
            $table->string('gender')->nullable();
            $table->string('country_id')->nullable();
            $table->string('address')->nullable();
            $table->string('email')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('arrival_date')->nullable();
            $table->string('departure_date')->nullable();
            $table->string('number_adults')->nullable();
            $table->string('number_kids')->nullable();
            $table->string('nights_at_hotel')->nullable();
            $table->string('expenses_cost')->nullable();
            $table->string('selling_cost')->nullable();
            $table->string('additional_cost')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
