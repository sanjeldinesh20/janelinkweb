<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_name')->nullable();
            $table->string('age')->nullable();            
            $table->string('dob')->nullable();            
            $table->string('employee_image')->nullable();
            $table->string('gender')->nullable();
            $table->string('employee_address')->nullable();
            $table->string('employee_email')->nullable();
            $table->string('employee_phone')->nullable();
            $table->string('employee_passport')->nullable();
            $table->string('hired_date')->nullable();            
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
