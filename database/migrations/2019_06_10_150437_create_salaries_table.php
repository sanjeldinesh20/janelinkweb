<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->increments('id');
             $table->string('employee_id')->nullable();
            $table->string('month_id')->nullable();
            $table->string('basic_salary')->nullable();
            $table->string('advance_id')->nullable();
            $table->string('leave_taken')->nullable();
            
            $table->string('total')->nullable();
            $table->string('description')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salaries');
    }
}
