<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('organization_name')->nullable();
            $table->string('organization_logo')->nullable();
            $table->string('organization_email')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('organization_address')->nullable();
            $table->string('chief')->nullable();
            $table->string('director')->nullable();
            $table->string('tax')->nullable();
            $table->string('organization_facebook')->nullable();
            $table->string('organization_twitter')->nullable();
            $table->string('organization_instagram')->nullable();
            $table->string('organization_linkedin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_settings');
    }
}
