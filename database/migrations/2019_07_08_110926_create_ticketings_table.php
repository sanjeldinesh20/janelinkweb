<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticketings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('way_type')->nullable();
            $table->string('book_date')->nullable();
            $table->string('service_id')->nullable();
            $table->string('full_name')->nullable();
            $table->string('flight_date')->nullable();
            $table->string('flight_time')->nullable();
            $table->string('fromcountryName')->nullable();
            $table->string('fromairportName')->nullable();
            $table->string('tocountryName')->nullable();
            $table->string('toairportName')->nullable();
            $table->string('number_passenger')->nullable();
            $table->string('fullname')->nullable();
            $table->string('gender')->nullable();
            $table->string('age')->nullable();
            $table->string('passportno')->nullable();
            $table->string('phoneno')->nullable();
            $table->string('email')->nullable();
            $table->string('number_kids')->nullable();
            $table->string('arilines')->nullable();
            $table->string('country_id')->nullable();
            $table->string('address')->nullable();
            $table->string('flightnumber')->nullable();
            $table->string('fromto')->nullable();
            $table->string('flightdate')->nullable();
            $table->string('timeofdeparture')->nullable();
            $table->string('arrivaltime')->nullable();
            $table->string('sts')->nullable();
            $table->string('expenses_cost')->nullable();
            $table->string('selling_cost')->nullable();
            $table->string('additional_cost')->nullable();
            $table->string('employee_id')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticketings');
    }
}
